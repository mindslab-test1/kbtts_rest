package brain.api.tts.data;

import brain.api.common.data.CommonMsg;

import java.util.List;

public class TtsResUsage {
    private CommonMsg message;
    private Integer count;
    private List result;

    public TtsResUsage(Integer count, List result) {
        this.count = count;
        this.result = result;
        this.message = new CommonMsg();
    }
    public TtsResUsage(CommonMsg message) {
        this.count = null;
        this.result = null;
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Integer getCount() {
        return count;
    }
    public void setCount(Integer count) {
        this.count = count;
    }

    public List getResult() {
        return result;
    }
    public void setResult(List result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "TtsResUsage{" +
                "message=" + message +
                ", count='" + count + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
