package brain.api.stt.data;

import brain.api.common.data.CommonMsg;

import java.util.List;

public class StreamCnnSttRes {
    private CommonMsg commonMsg;
    private List<TextSegment> result;

    public StreamCnnSttRes(List<TextSegment> result) {
        this.commonMsg = new CommonMsg();
        this.result = result;
    }

    public StreamCnnSttRes(CommonMsg commonMsg) {
        this.commonMsg = commonMsg;
        this.result = null;
    }

    public CommonMsg getCommonMsg() {
        return commonMsg;
    }
    public void setCommonMsg(CommonMsg commonMsg) {
        this.commonMsg = commonMsg;
    }

    public List<TextSegment> getResult() {
        return result;
    }
    public void setResult(List<TextSegment> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "StreamCnnSttRes{" +
                "commonMsg=" + commonMsg +
                ", result=" + result +
                '}';
    }
}
