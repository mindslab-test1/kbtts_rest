package brain.api.animeface;

public class AnimeFaceCommonCode {
    public static final String SERVICE_NAME = "AnimeFace";
    public static final String IP = "182.162.19.6";
    public static final int PORT_NEW_SAMPLE_STYLE_EDIT = 39989;
    public static final int PORT_FACE_MAKE = 39990;

    private AnimeFaceCommonCode() {}
}
