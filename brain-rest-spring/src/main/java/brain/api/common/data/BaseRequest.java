package brain.api.common.data;

/*
    request base for api calls
 */

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseRequest implements Serializable {
    private String apiId;
    private String apiKey;

    protected void setApiIdKey(String apiId, String apiKey){
        this.apiId = apiId;
        this.apiKey = apiKey;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiId(){
        return apiId;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey(){
        return apiKey;
    }


}
