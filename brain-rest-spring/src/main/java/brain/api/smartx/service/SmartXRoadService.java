package brain.api.smartx.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.service.BaseService;
import brain.api.smartx.SmartXCode;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class SmartXRoadService extends BaseService {

    final String smartXUrl = SmartXCode.SMARTX_ADDRESS;

    public Map<String, Object> getplateRecog(MultipartFile car_img, String apiId, String apiKey, HttpServletRequest request){
        String url = smartXUrl + SmartXCode.PLATE_RECOG;
        Map<String, Object> map = new LinkedHashMap<>();

        try{
            validate(apiId, apiKey, SmartXCode.PLATE_RECOG, 1, request);

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            //File avrFile = new File(uploadPath);
            File avrFile = Files.createTempDir();

            avrFile = new File((avrFile.getPath() + "/" + car_img.getOriginalFilename().substring(car_img.getOriginalFilename().lastIndexOf("\\") + 1)));
            car_img.transferTo(avrFile);

            FileBody avrFileBody = new FileBody(avrFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("car_img", avrFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if(responseCode != 200){
                throw new RuntimeException("ErrCode : "+response);
            }

            InputStream in = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            int count = multipart.getCount();
            String name = "";

            for(int i=0;i<count;i++){
                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                name = getNameFromHeader(bodyPart);

                if(bodyPart.isMimeType("text/plain")){
                    System.out.println("MIME type :: text/plain");
                    BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream(), "UTF-8"));
                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while((line=rd.readLine()) != null){
                        result.append(line);
                    }
                    String strResult = result.toString();

                    map.put(name, strResult);
                }else if(bodyPart.isMimeType("image/jpeg")){
                    System.out.println("MIME type :: image/jpeg");
                    InputStream multiIn =bodyPart.getInputStream();
                    byte[] resultImage = IOUtils.toByteArray(multiIn);

                    map.put(name, resultImage);
                }else{
                    System.out.println("else ");
                }
            }
            avrFile.delete();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        //return map;
        return map;
    }

    public Map<String, Object> getPoseRecog(MultipartFile img, String apiId, String apiKey, HttpServletRequest request){
        Map<String, Object> map = new LinkedHashMap<>();
        String url = smartXUrl + SmartXCode.POSE_EXTRACT;

        try{
            validate(apiId, apiKey, SmartXCode.POSE_EXTRACT, 1, request);

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File avrFile = Files.createTempDir();

            avrFile = new File((avrFile.getPath() + "/" + img.getOriginalFilename().substring(img.getOriginalFilename().lastIndexOf("\\") + 1)));
            img.transferTo(avrFile);

            FileBody avrFileBody = new FileBody(avrFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addPart("pose_img", avrFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if (responseCode != 200) {
                throw new RuntimeException("ErrCode : " + response);
            }

            InputStream in = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            for(int i=0;i<multipart.getCount();i++){

                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                String name = getNameFromHeader(bodyPart);

                if(bodyPart.isMimeType("image/png") || bodyPart.isMimeType("image/jpeg")){
                    InputStream multiIn =bodyPart.getInputStream();
                    byte[] resultImage = IOUtils.toByteArray(multiIn);
                    map.put(name, resultImage);
                }else{
                }
            }
            avrFile.delete();
        }catch (Exception e){
            e.printStackTrace();
        }

        return map;
    }

    public Map<String, Object> getAnomalyDetect(MultipartFile video, String apiId, String apiKey, HttpServletRequest request){
        Map<String, Object> map = new LinkedHashMap<>();
        String url = smartXUrl + SmartXCode.ANOM_DETECT;

        try {
            validate(apiId, apiKey, SmartXCode.ANOM_DETECT, 1, request);

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            //File AnomalyFile = new File(uploadPath);
            File AnomalyFile = Files.createTempDir();

            AnomalyFile = new File((AnomalyFile.getPath() + "/" + video.getOriginalFilename().substring(video.getOriginalFilename().lastIndexOf("\\") + 1)));
            video.transferTo(AnomalyFile);

            FileBody AnomalyFileBody = new FileBody(AnomalyFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addPart("video", AnomalyFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if (responseCode != 200) {
                throw new RuntimeException("ErrCode : " + response);
            }

            InputStream in = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            for(int i=0;i<multipart.getCount();i++){

                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                String name = getNameFromHeader(bodyPart);

                if(bodyPart.isMimeType("text/plain")){
                    BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream()));
                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while((line=rd.readLine()) != null){
                        result.append(line);
                    }
                    String strResult = result.toString();
                    map.put(name, strResult);
                }else if(bodyPart.isMimeType("image/jpeg")){
                    InputStream multiIn =bodyPart.getInputStream();
                    byte[] resultImage = IOUtils.toByteArray(multiIn);
                    map.put(name, resultImage);
                }else{
                }
            }
            AnomalyFile.delete();
        }catch(Exception e){
            System.out.println(e);
        }

        return map;
    }

    String getNameFromHeader(BodyPart bodyPart){
        String result = "";

        try {
            Enumeration<Header> enumeration = bodyPart.getAllHeaders();
            while (enumeration.hasMoreElements()) {
                Header next = enumeration.nextElement();
                String name = next.getName();
                String value = next.getValue();

                if("Content-Disposition".equals(name)){
                    String[] headers = value.split(" ");
                    for(String header : headers){
                        if("name".equals(header.split("=")[0])){
                            result = header.split("=")[1].replaceAll("\"", "").replaceAll(";", "");
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

}
