package brain.api.segment.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.segment.SegmentCommonCode;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.u2net.U2NetGrpc;
import maum.brain.u2net.U2NetOuterClass.PngFileBinary;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class SegmentClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(SegmentCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    private U2NetGrpc.U2NetStub segmentStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.segmentStub = U2NetGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public ByteArrayOutputStream streamSegment(MultipartFile file) throws Exception {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // STREAM RESPONSE
        StreamObserver<PngFileBinary> requestObserver = segmentStub.streamU2Net(new StreamObserver<PngFileBinary>() {
                @Override
                public void onNext(PngFileBinary pngFileBinary) {
                    try {
                        outputStream.write(pngFileBinary.getBin().toByteArray());
                    } catch (IOException e) {
                        logger.info("Error: Stream Response from streamSegment");
                        throw new InternalException(e.getMessage());
                    }
                }

                @Override
                public void onError(Throwable t) {
                    logger.info("Error: Stream Response onError from streamSegment");
                    logger.error(t.getMessage());
                    finishLatch.countDown();
                }

                @Override
                public void onCompleted() {
                    logger.info("Finished: Stream Response from streamSegment");
                    finishLatch.countDown();
                }
            });

        // STREAM REQUEST
        try {
            PngFileBinary.Builder pngFileBinaryBuilder = PngFileBinary.newBuilder();
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024*1024];
            while ((len = bis.read(buffer)) > 0){
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(pngFileBinaryBuilder.setBin(inputByteString).build());
            }
        } catch (RuntimeException | IOException e) {
            logger.info("Error: Stream request from streamSegment");
            requestObserver.onError(e);
            throw new InternalException(e.getMessage());
        }
        requestObserver.onCompleted();

        finishLatch.await(1, TimeUnit.MINUTES);

        return outputStream;
    }
}
