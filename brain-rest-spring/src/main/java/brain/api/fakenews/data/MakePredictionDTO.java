package brain.api.fakenews.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MakePredictionDTO {
    private String apiId;
    private String apiKey;
    private String title;
    private String description;
}
