package brain.api.dap.client;

import brain.api.common.data.CommonMsg;
import brain.api.dap.data.DapBaseResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface DapClientInterface {
    public DapBaseResponse diarizeWav(MultipartFile inputFile);
    public DapBaseResponse diarizeWavStream(InputStream wavData);
    public DapBaseResponse dVectorizeWav(MultipartFile inputFile);
    public DapBaseResponse dVectorizeWavStream(InputStream wavData);
    public DapBaseResponse cnnNoise(MultipartFile inputFile);
    public CommonMsg setDestination(String ip, int port);
}
