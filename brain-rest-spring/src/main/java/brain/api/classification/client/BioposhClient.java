package brain.api.classification.client;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.data.FaceConditionResponse;
import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.bioposh_classification.Bioposh;
import maum.brain.bioposh_classification.BioposhClsGrpc;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static brain.api.classification.ClassificationCommonCode.*;

@Component
public class BioposhClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final Map<String, BioposhClient.ChannelHolder> channelMap = new HashMap<>();

    private BioposhClsGrpc.BioposhClsStub bioposhClsStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();

            String channelKey = String.format("set destination %s, %d", ip, port);
            logger.debug(channelKey);

            ManagedChannel channel = CommonUtils.getManagedChannel(ip, port);
            this.bioposhClsStub = BioposhClsGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

            channelMap.put(channelKey, new ChannelHolder(
                    channel, System.currentTimeMillis()
            ));

            return new CommonMsg();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        } finally {
            this.manageChannel();
        }
    }

    public FaceConditionResponse classifyImage(byte[] frontBytes, byte[] leftBytes, byte[] rightBytes) throws MindsRestException {
        ManagedChannel channel;
        String channelKey = String.format("set destination %s, %d", super.destIp, super.destPort);
        logger.debug(channelKey);
        if (channelMap.containsKey(channelKey)) {
            channel = channelMap.get(channelKey).channel;
            channelMap.get(channelKey).lastReferencedEpoch = System.currentTimeMillis();
        } else {
            channel = CommonUtils.getManagedChannel(super.destIp, super.destPort);
            channelMap.put(channelKey, new ChannelHolder(
                    channel, System.currentTimeMillis()
            ));
        }

        final CountDownLatch finishLatch = new CountDownLatch(1);

        FaceConditionResponse faceConditionResponse = new FaceConditionResponse();

        StreamObserver<Bioposh.Input> requestObserver = bioposhClsStub.determinate(new StreamObserver<Bioposh.Output>() {
            @Override
            public void onNext(Bioposh.Output output) {
                faceConditionResponse.setResultList(output.getAllResultList().stream().map(clsResult -> {
                    FaceConditionResponse.Result result = new FaceConditionResponse.Result();
                    result.setCategory(clsResult.getCategory());
                    result.setPred(clsResult.getPred());
                    return result;
                }).collect(Collectors.toList()));
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Error: Stream Response onError from streamFaceClassification");
                logger.error(throwable.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Stream Response from streamFaceClassification");
                finishLatch.countDown();
            }
        });

        // Stream Request (Image)
        Bioposh.Input.Builder builder = Bioposh.Input.newBuilder();

        // TODO: Refactoring Target => parallel로 병렬 처리
        handleByteString(requestObserver, builder, frontBytes, FRONT_IMAGE);
        handleByteString(requestObserver, builder, leftBytes, LEFT_IMAGE);
        handleByteString(requestObserver, builder, rightBytes, RIGHT_IMAGE);

        requestObserver.onCompleted();

        boolean timeout;

        // 2. Waiting till async work is done
        try {
            timeout = finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e);
        }

        if (!timeout) {
            logger.error("gRPC async request timeout");
            throw new MindsRestException();
        }

        logger.info(faceConditionResponse);

        return faceConditionResponse;
    }

    private void handleByteString(StreamObserver<Bioposh.Input> requestObserver, Bioposh.Input.Builder builder, byte[] bytes, String type) throws MindsRestException {
        // try - catch - resource 방식 (autoClosable 구현체라 inputStream을 안닫아도 됨)
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes)) {
            int len;
            byte[] buffer = new byte[1024 * 512];
            while ((len = bis.read(buffer)) > 0) {
                ByteString inputByteString;
                if (buffer.length > len) { // 파일 마지막의 크기가 기준보다 작으면
                    inputByteString = ByteString.copyFrom(Arrays.copyOfRange(buffer, 0, len - 1));
                } else {
                    inputByteString = ByteString.copyFrom(buffer);
                }
                Bioposh.Input.Builder typeBuilder;
                switch (type) {
                    case FRONT_IMAGE:
                        typeBuilder = builder.setFront(inputByteString);
                        break;
                    case LEFT_IMAGE:
                        typeBuilder = builder.setLeft(inputByteString);
                        break;
                    case RIGHT_IMAGE:
                        typeBuilder = builder.setRight(inputByteString);
                        break;
                    default:
                        throw new MindsRestException("Unsupported image type");
                }
                requestObserver.onNext(typeBuilder.build());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    private static class ChannelHolder {
        public ManagedChannel channel;
        public long lastReferencedEpoch;

        ChannelHolder(ManagedChannel channel, long lastReferencedEpoch) {
            this.channel = channel;
            this.lastReferencedEpoch = lastReferencedEpoch;
        }
    }

    private void manageChannel() {
        logger.debug("manage channel");
        if (channelMap.size() > 10) {
            logger.debug("maintained channel over 10");
            String channelKey = "";
            long latestReferencedTime = System.currentTimeMillis();
            for (Map.Entry<String, BioposhClient.ChannelHolder> entry : channelMap.entrySet()) {
                if (entry.getValue().lastReferencedEpoch < latestReferencedTime) {
                    channelKey = entry.getKey();
                    latestReferencedTime = entry.getValue().lastReferencedEpoch;
                }
            }
            ManagedChannel removeTargetChannel = channelMap.get(channelKey).channel;
            channelMap.remove(channelKey);
            removeTargetChannel.shutdown();
            logger.debug(String.format("removed %s", channelKey));
        }
        logger.debug(String.format("channel map size %d", channelMap.size()));
    }

    @PreDestroy
    private void onDestroy() {
        for (Map.Entry<String, BioposhClient.ChannelHolder> entry : channelMap.entrySet()) {
            entry.getValue().channel.shutdown();
        }
    }
}
