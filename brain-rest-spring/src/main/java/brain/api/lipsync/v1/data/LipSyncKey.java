package brain.api.lipsync.v1.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LipSyncKey implements Serializable {
    private String requestKey;

    public LipSyncKey() {
    }

    public LipSyncKey(String requestKey) {
        this.requestKey = requestKey;
    }

    public String getRequestKey() {
        return requestKey;
    }

    public void setRequestKey(String requestKey) {
        this.requestKey = requestKey;
    }

    @Override
    public String toString() {
        return "LipSyncKey{" +
                "requestKey='" + requestKey + '\'' +
                '}';
    }
}
