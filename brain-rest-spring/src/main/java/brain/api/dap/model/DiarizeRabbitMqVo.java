package brain.api.dap.model;

import brain.api.common.model.drama.RabbitMqDto;

public class DiarizeRabbitMqVo extends RabbitMqDto {

    public DiarizeRabbitMqVo() {
        setQueue_name("diarize_actor");
        setActor_name("diarize_actor");
    }
}
