package brain.api.legacy.data.mrc;

public class MrcRequestLegacy {

    private String ID;
    private String key;
    private String cmd;
    private String lang;
    private String sentence;
    private String question;

    public MrcRequestLegacy() {
    }

    public MrcRequestLegacy(String ID, String key, String cmd, String lang, String sentence, String question) {
        this.ID = ID;
        this.key = key;
        this.cmd = cmd;
        this.lang = lang;
        this.sentence = sentence;
        this.question = question;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCmd() {
        return this.cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getSentence() {
        return this.sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "MrcRequestLegacy{" +
                "ID='" + ID + '\'' +
                ", key='" + key + '\'' +
                ", cmd='" + cmd + '\'' +
                ", lang='" + lang + '\'' +
                ", sentence='" + sentence + '\'' +
                ", question='" + question + '\'' +
                '}';
    }
}
