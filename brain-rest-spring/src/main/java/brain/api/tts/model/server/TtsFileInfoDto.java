package brain.api.tts.model.server;

import brain.api.tts.TtsCommonCode;

import java.util.Date;

public class TtsFileInfoDto {
    private int id;
    private String fileId;
    private String makerId;
    private String voiceName;
    private String ip;
    private Date createDate;
    private Date expiryDate;
    private int status;

    public TtsFileInfoDto() {
        // Nothing to Do
    }

    public TtsFileInfoDto(String fileId, String makerId, String voiceName, String ip, Date createDate) {
        this.fileId = fileId;
        this.makerId = makerId;
        this.voiceName = voiceName;
        this.ip = ip;
        this.createDate = createDate;
        this.expiryDate = new Date(createDate.getTime() + ((1000 * 60 * 60 * 24) * TtsCommonCode.TTS_FILE_EXPIRY_DAY));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TtsFileInfoDto{" +
                "id=" + id +
                ", fileId='" + fileId + '\'' +
                ", makerId='" + makerId + '\'' +
                ", voiceName='" + voiceName + '\'' +
                ", ip='" + ip + '\'' +
                ", createDate=" + createDate +
                ", expiryDate=" + expiryDate +
                ", status=" + status +
                '}';
    }
}
