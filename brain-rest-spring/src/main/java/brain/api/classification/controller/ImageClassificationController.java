package brain.api.classification.controller;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.data.dto.FaceConditionDto;
import brain.api.classification.data.dto.GenuineFakeDto;
import brain.api.classification.service.ImageClassificationService;
import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/classification/image")
public class ImageClassificationController {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final ImageClassificationService imageClassificationService;

    /**
     * Triple.E
     */
    @RequestMapping(
            value = "/genuine-fake",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> classifyForGenuineFake(
            GenuineFakeDto genuineFakeDto,
            HttpServletRequest httpServletRequest
    ) {
        logger.debug(genuineFakeDto);
        try {
            return ResponseEntity.ok(imageClassificationService.classify(genuineFakeDto, httpServletRequest));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Bioposh
     */
    @RequestMapping(
            value = "/face-condition",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> classifyForFaceCondition(
            FaceConditionDto faceConditionDto,
            HttpServletRequest httpServletRequest
    ) {
        logger.debug(faceConditionDto);
        try {
            return ResponseEntity.ok(imageClassificationService.classify(faceConditionDto, httpServletRequest));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
