package brain.api.ner.model;

public class NerRequestParams {
    private String apiId;
    private String apiKey;
    private String context;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getContext() { return context; }
    public void setContext(String context) { this.context = context; }

    @Override
    public String toString() {
        return "GptRequestParams{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", context='" + context + '\'' +
                '}';
    }
}
