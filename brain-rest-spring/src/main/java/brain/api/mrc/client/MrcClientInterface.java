package brain.api.mrc.client;

import brain.api.common.data.CommonMsg;
import brain.api.mrc.data.MrcBaseResponse;

public interface MrcClientInterface {
    public MrcBaseResponse bertSquadMrc(String context, String question);
    public CommonMsg setDestination(String ip, int port);
}
