package brain.db.mapper;

import brain.api.classification.data.model.ImageClassificationModelInfo;

public interface ImageClassificationModelInfoMapper {
    ImageClassificationModelInfo findByModel(String model);
}
