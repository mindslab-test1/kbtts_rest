package brain.api.konglish.service;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.konglish.KonglishCommonCode;
import brain.api.konglish.client.KonglishClient;
import brain.api.konglish.data.KonglishResSentence;
import brain.api.konglish.data.KonglishResWords;
import brain.api.konglish.model.KonglishReqSentenceParams;
import brain.api.konglish.model.KonglishReqWordsParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class KonglishService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(KonglishCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    KonglishClient konglishClient;

    public KonglishResWords konglishWords(KonglishReqWordsParams params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), KonglishCommonCode.SERVICE_NAME, params.getWordList().size(), request);
        konglishClient.setDestination("114.108.173.100", 30999);

        return konglishClient.doKonglishWords(params.getWordList());
    }

    public KonglishResSentence konglishSentence(KonglishReqSentenceParams params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), KonglishCommonCode.SERVICE_NAME, params.getSentence().length(), request);
        konglishClient.setDestination("114.108.173.100",30999);

        return new KonglishResSentence(konglishClient.doKonglishSentence(params.getSentence()));
    }


}
