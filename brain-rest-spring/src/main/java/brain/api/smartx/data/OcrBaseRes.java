package brain.api.smartx.data;

import java.util.Arrays;

public class OcrBaseRes {
    private String name;
    private byte[] data;

    public OcrBaseRes(String name, byte[] data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }
    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OcrBaseRes{" +
                "name='" + name + '\'' +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
