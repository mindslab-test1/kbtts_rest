package brain.api.admin.service;

import brain.api.admin.AdminCommonCode;
import brain.api.common.CommonCode;
import brain.api.common.data.BaseResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.server.ServerDto;
import brain.api.common.model.usage.UsageDto;
import brain.api.common.model.user.ApiClientDto;
import brain.api.common.model.user.DjangoClientDto;
import brain.api.common.service.BaseService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class AdminService extends BaseService {

    private static final CloudApiLogger logger = new CloudApiLogger("Admin", CommonCode.COMMON_CLASSNAME_SERVICE);

    @Override
    public List<ServerDto> getServerByService(String service) {
        return null;
    }

    public BaseResponse addClient(ApiClientDto dto) throws MindsRestException {
        if(apiClientDao.getApiClientInfo(dto.getApiId()) != null) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "The apiID already exists. : " + dto.getApiId());
        }

        String apiKey = UUID.randomUUID().toString().replaceAll("-", "");
        logger.info("API Key Issuanced - apiId : "+ dto.getApiId() + ", apiKey : " + apiKey);

        DjangoClientDto djangoClientDto = new DjangoClientDto(dto.getName(), dto.getEmail(), dto.getApiId(), apiKey);
        dto.setApiKey(apiKey);

        if(apiClientDao.setNewApiClient(dto) + apiClientDao.insertDjangoClient(djangoClientDto) > 1){
            return new BaseResponse(dto);
        }
        else throw new MindsRestException();
    }

    public List<UsageDto> selectUsage(Map<String, String> input, HttpServletRequest request) throws MindsRestException {
        String service = input.get("service");
        String apiId = input.get("apiId");

        String admin = validate(apiId, input.get("apiKey"), AdminCommonCode.SERVICE_NAME, 1, request);
        logger.info(admin);

        if (input.get("startDate") != null && input.get("endDate") != null
                && input.get("startDate").compareTo(input.get("endDate")) == 1) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "The startDate cannot be greater than the endDate.");
        }

        if (service == null) service = "empty";

        if (!admin.equals(CommonCode.API_CLIENT_ADMIN_MSG)) {
            throw new MindsRestException(AdminCommonCode.ADMIN_ERR_FORBIDDEN, AdminCommonCode.ADMIN_MSG_FORBIDDEN);
        }

        logger.debug(service);
        List<UsageDto> usages = apiClientDao.selectUsage(input);

        if (service.equals("empty") || service.equalsIgnoreCase("STT")) {
            List<UsageDto> sttUsage = apiClientDao.selectSttusage(input);
            logger.debug(sttUsage.size() + ", apiId : " + input.get("reqId"));
            usages.addAll(sttUsage);
        }

        logger.info("selectUsage - searcher : " + apiId + ", reqId : " + input.get("reqId") + ", usageNum : " + usages.size());
        return usages;
    }

    public int deleteClient(String apiId) {
        ApiClientDto apiClientDto = new ApiClientDto(apiId, CommonCode.API_CLIENT_DISABLE);
        logger.debug(apiClientDto);
        return apiClientDao.updateClient(apiClientDto);
    }

    public int resumeClient(String apiId) {
        ApiClientDto apiClientDto = new ApiClientDto(apiId, CommonCode.API_CLIENT_ENABLE);
        logger.debug(apiClientDto);
        return apiClientDao.updateClient(apiClientDto);
    }

    public BaseResponse addDjangoClient(DjangoClientDto clientDto) throws MindsRestException {
        if (apiClientDao.insertDjangoClient(clientDto) > 0) {
            return new BaseResponse();
        } else {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    public ApiClientDto getClientInfo(Map<String, String> input, HttpServletRequest request) throws MindsRestException {
        validate(input.get("apiId"), input.get("apiKey"), AdminCommonCode.SERVICE_NAME, 1, request);
        ApiClientDto apiClientDto = apiClientDao.getApiClientInfo(input.get("reqId"));
        apiClientDto.setApiKey(null);
        if (apiClientDto == null) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
        }
        return apiClientDto;
    }
}
