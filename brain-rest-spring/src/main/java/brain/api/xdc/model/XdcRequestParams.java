package brain.api.xdc.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcRequestParams implements Serializable {

    private String apiId;
    private String apiKey;
    private String context;

    public XdcRequestParams() {
    }

    public XdcRequestParams(String apiId, String apiKey, String context) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.context = context;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "XdcRequestParams{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", context='" + context + '\'' +
                '}';
    }
}
