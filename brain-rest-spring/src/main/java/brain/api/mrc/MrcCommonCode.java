package brain.api.mrc;

public class MrcCommonCode {
    private MrcCommonCode(){}

    public static final String SERVICE_NAME = "MRC";

    // Success
    public static final int MRC_RES_SUCCESS = 0;

    // Communication related error
    public static final int MRC_ERR_CONN_REFUSED = 50001;
    public static final int MRC_ERR_CONN_TIMEOUT = 50002;
    public static final int MRC_ERR_REQ_TIMEOUT = 50003;

    public static final int MRC_ERR_AUTH_VERIFICATION_ERROR = 40001;

}
