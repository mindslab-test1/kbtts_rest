package brain.api.common.model.usage;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsageDto {
    private String service;
    private String apiId;
    private int usage;
    private String userIp;
    private String deviceType;
    private String appName;
    private String osName;
    private String clientType;
    private String date;

    public UsageDto() {
    }

    public UsageDto(String service, String apiId, int usage) {
        this.service = service;
        this.apiId = apiId;
        this.usage = usage;
        this.userIp = null;
        this.deviceType = null;
        this.appName = null;
        this.osName = null;
        this.clientType = null;
    }

    public UsageDto(
            String service,
            String apiId,
            int usage,
            String userIp,
            String deviceType,
            String appName,
            String osName,
            String clientType
    ) {
        this.service = service;
        this.apiId = apiId;
        this.usage = usage;
        this.userIp = userIp;
        this.deviceType = deviceType;
        this.appName = appName;
        this.osName = osName;
        this.clientType = clientType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public int getUsage() {
        return usage;
    }

    public void setUsage(int usage) {
        this.usage = usage;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UsageDto{" +
                "service='" + service + '\'' +
                ", apiId='" + apiId + '\'' +
                ", usage=" + usage +
                ", userIp='" + userIp + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", appName='" + appName + '\'' +
                ", osName='" + osName + '\'' +
                ", clientType='" + clientType + '\'' +
                '}';
    }
}
