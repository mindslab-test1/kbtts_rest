package brain.api.classification.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class FaceConditionDto {
    private String apiId;
    private String apiKey;
    private String model;
    private MultipartFile front;
    private MultipartFile left;
    private MultipartFile right;
}
