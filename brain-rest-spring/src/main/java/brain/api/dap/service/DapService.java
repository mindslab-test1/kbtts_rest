package brain.api.dap.service;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.dap.DapCommonCode;
import brain.api.dap.client.DapDramaClient;
import brain.api.dap.client.VoiceRecogServiceGrpcClient;
import brain.api.dap.data.recog.DeleteVoiceResponse;
import brain.api.dap.data.recog.GetVoiceResponse;
import brain.api.dap.data.recog.SetVoiceResponse;
import brain.api.dap.data.recog.VoiceRecogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DapService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(DapCommonCode.SERVICE_APP_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    DapDramaClient dapClient;

    @Autowired
    VoiceRecogServiceGrpcClient voiceRecogServiceClient;

    public SetVoiceResponse setVoice(String apiId, String apiKey, String dbId, String voiceId, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);
        setDestination();

        logger.debug("dap");
        List<Object> voiceVector = dapClient.doGhostvlad(file).getVector();

        logger.debug("dap_app");
        return voiceRecogServiceClient.setVoice(apiId, dbId, voiceId, voiceVector);
    }

    public GetVoiceResponse getVoiceList(String apiId, String apiKey, String dbId, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);
        setDestination();

        return voiceRecogServiceClient.getVoiceList(apiId, dbId);
    }

    public DeleteVoiceResponse deleteVoice(String apiId, String apiKey, String dbId, String voiceId, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);
        setDestination();

        return voiceRecogServiceClient.deleteVoice(apiId, dbId, voiceId);
    }

    public VoiceRecogResponse recogVoice(String apiId, String apiKey, String dbId, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);
        setDestination();


        List<Object> voiceVector = dapClient.doGhostvlad(file).getVector();
        return voiceRecogServiceClient.recogVoice(apiId, dbId, voiceVector);
    }

    private void setDestination(){
        voiceRecogServiceClient.setDestination("182.162.19.10",37650);
    }

    private List<Float> object2Float(List<Object> vector){
        return vector
            .stream()
            .map(e->(Float)e)
            .collect(Collectors.toList());
    }
}
