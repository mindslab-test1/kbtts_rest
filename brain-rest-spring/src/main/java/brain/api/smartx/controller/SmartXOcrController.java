package brain.api.smartx.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.data.OcrHospRes;
import brain.api.smartx.service.SmartFeatureService;
import brain.api.smartx.service.SmartXOcrService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/ocr")
public class SmartXOcrController {
    private static final CloudApiLogger logger = new CloudApiLogger(SmartXCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    SmartFeatureService smartFeatureService;
    @Autowired
    SmartXOcrService service;

    @PostMapping(
            value = "/hospitalReceipt",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public OcrHospRes hospitalReceipt(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("receipt_img") MultipartFile receipt_img,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        try {
            return service.hospitalReceipt(apiId, apiKey, receipt_img, request);
        } catch (MindsRestException | IOException e) {
            e.printStackTrace();
            return new OcrHospRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @PostMapping(
            value = "/dataSelection",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public String dataAnalysis(@RequestParam("var_file") MultipartFile var_file
            , @RequestParam("data_file") MultipartFile data_file
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartFeatureService.getFeatureSelection(var_file, data_file, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }
}
