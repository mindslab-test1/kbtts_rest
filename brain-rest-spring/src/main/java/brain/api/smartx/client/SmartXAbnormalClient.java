package brain.api.smartx.client;

import brain.api.common.CommonCode;
import brain.api.common.CommonMultipartFormDataHandler;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.data.ResponseAbnormalData;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class SmartXAbnormalClient {
    private final RestTemplate restTemplate;
    private final CommonMultipartFormDataHandler commonMultipartFormDataHandler;

    private static final CloudApiLogger logger = new CloudApiLogger(SmartXCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private HttpHeaders defaultRequestHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        return requestHeaders;
    }

    public ResponseAbnormalData detectFalldown(MultipartFile video, String roiList) throws MindsRestException, IOException, MessagingException {
        HttpHeaders requestHeaders = defaultRequestHeaders();

        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("video_source", video.getResource());
        requestBody.add("roi_list", roiList);

        HttpEntity<MultiValueMap<String, Object>> requestHttpEntity = new HttpEntity<>(requestBody, requestHeaders);
        ResponseEntity<String> responseEntity;
        try {
            String origin = SmartXCode.SMARTX_ABNORMAL_ADDRESS + ":" + SmartXCode.SMARTX_ABNORMAL_FALLDOWN_PORT;
            responseEntity = restTemplate.exchange(origin + "/hau/falldown", HttpMethod.POST, requestHttpEntity, String.class);
        } catch (RestClientException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e.getMessage());
        }

        Map<String, Object> map = commonMultipartFormDataHandler.parse(responseEntity.getBody().getBytes());

        ResponseAbnormalData responseAbnormalData = new ResponseAbnormalData();
        responseAbnormalData.setStatus((String) map.get("status"));
        responseAbnormalData.setAbnormalImg(
                (map.get("abnormal_img").equals(StringUtils.EMPTY))?
                        StringUtils.EMPTY.getBytes(): (byte[]) map.get("abnormal_img")
        );
        logger.info("Abnormal Result: " + responseAbnormalData);
        return responseAbnormalData;
    }

    public ResponseAbnormalData detectLoitering(MultipartFile video, String roiList) throws MindsRestException, IOException, MessagingException {
        HttpHeaders requestHeaders = defaultRequestHeaders();

        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("video_source", video.getResource());
        requestBody.add("roi_list", roiList);

        HttpEntity<MultiValueMap<String, Object>> requestHttpEntity = new HttpEntity<>(requestBody, requestHeaders);
        ResponseEntity<String> responseEntity;
        try {
            String origin = SmartXCode.SMARTX_ABNORMAL_ADDRESS + ":" + SmartXCode.SMARTX_ABNORMAL_LOITERING_PORT;
            responseEntity = restTemplate.exchange(origin + "/hau/loitering", HttpMethod.POST, requestHttpEntity, String.class);
        } catch (RestClientException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e.getMessage());
        }

        Map<String, Object> map = commonMultipartFormDataHandler.parse(responseEntity.getBody().getBytes());

        ResponseAbnormalData responseAbnormalData = new ResponseAbnormalData();
        responseAbnormalData.setStatus((String) map.get("status"));
        responseAbnormalData.setAbnormalImg(
                (map.get("abnormal_img").equals(StringUtils.EMPTY))?
                        StringUtils.EMPTY.getBytes(): (byte[]) map.get("abnormal_img")
        );
        logger.info("Abnormal Result: " + responseAbnormalData);
        return responseAbnormalData;
    }

    public ResponseAbnormalData detectIntrusion(MultipartFile video, String roiList) throws MindsRestException, IOException, MessagingException {
        HttpHeaders requestHeaders = defaultRequestHeaders();

        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("video_source", video.getResource());
        requestBody.add("roi_list", roiList);

        HttpEntity<MultiValueMap<String, Object>> requestHttpEntity = new HttpEntity<>(requestBody, requestHeaders);
        ResponseEntity<String> responseEntity;
        try {
            String origin = SmartXCode.SMARTX_ABNORMAL_ADDRESS + ":" + SmartXCode.SMARTX_ABNORMAL_INTRUSION_PORT;
            responseEntity = restTemplate.exchange(origin + "/hau/intrusion", HttpMethod.POST, requestHttpEntity, String.class);
        } catch (RestClientException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e.getMessage());
        }

        Map<String, Object> map = commonMultipartFormDataHandler.parse(responseEntity.getBody().getBytes());

        ResponseAbnormalData responseAbnormalData = new ResponseAbnormalData();
        responseAbnormalData.setStatus((String) map.get("status"));
        responseAbnormalData.setAbnormalImg(
                (map.get("abnormal_img").equals(StringUtils.EMPTY))?
                        StringUtils.EMPTY.getBytes(): (byte[]) map.get("abnormal_img")
        );
        logger.info("Abnormal Result: " + responseAbnormalData);
        return responseAbnormalData;
    }
}
