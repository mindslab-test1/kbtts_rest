package brain.api.slomo.dto;

import brain.api.common.data.CommonMsg;
import brain.api.slomo.data.ProcessStatus;

public class QueryResponse {
    private CommonMsg message;
    private ProcessStatus payload;

    public QueryResponse() {
    }

    public QueryResponse(CommonMsg message, ProcessStatus payload) {
        this.message = message;
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public ProcessStatus getPayload() {
        return payload;
    }

    public void setPayload(ProcessStatus payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "QueryResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
