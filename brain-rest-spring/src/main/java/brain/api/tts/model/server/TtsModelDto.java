package brain.api.tts.model.server;

public class TtsModelDto {

    private String lang;
    private String model;
    private String ip;
    private int port;
    private int samplerate;
    private int speaker;
    private int maxSpeaker;

    public TtsModelDto() {

    }

    public TtsModelDto(String lang, String ip, int port, int samplerate, int speaker) {
        this.lang = lang;
        this.ip = ip;
        this.port = port;
        this.samplerate = samplerate;
        this.speaker = speaker;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
    }

    public int getSpeaker() {
        return speaker;
    }

    public void setSpeaker(int speaker) {
        this.speaker = speaker;
    }

    public int getMaxSpeaker() {
        return maxSpeaker;
    }

    public void setMaxSpeaker(int maxSpeaker) {
        this.maxSpeaker = maxSpeaker;
    }

    public TtsModelDto baseLine(String lang) {
        if(lang.equals("en_US")) {
            this.lang = lang;
            this.model = "baseLine";
            this.ip = "114.108.173.101";
            this.port = 30798;
            this.samplerate = 22050;
            this.speaker = 0;

            return this;
        }
        this.lang = lang;
        this.model = "baseLine";
        this.ip = "114.108.173.100";
        this.port = 30799;
        this.samplerate = 22050;
        this.speaker = 0;

        return this;
    }

    public TtsModelDto korKids(Integer speaker){
        this.lang = "ko_KR";
        this.model = "kor_kids";
        this.ip = "114.108.173.101";
        this.port = 30998;
        this.samplerate = 22050;
        this. speaker = speaker;

        return this;
    }

    @Override
    public String toString() {
        return "TtsModelDto{" +
                "lang='" + lang + '\'' +
                ", model='" + model + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", samplerate=" + samplerate +
                ", speaker=" + speaker +
                ", maxSpeaker=" + maxSpeaker +
                '}';
    }
}
