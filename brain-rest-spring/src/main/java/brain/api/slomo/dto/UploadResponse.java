package brain.api.slomo.dto;

import brain.api.common.data.CommonMsg;
import brain.api.slomo.data.UploadStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UploadResponse implements Serializable {
    private CommonMsg message;
    private UploadStatus payload;

    public UploadResponse() {
    }

    public UploadResponse(CommonMsg message, UploadStatus payload) {
        this.message = message;
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public UploadStatus getPayload() {
        return payload;
    }

    public void setPayload(UploadStatus payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "UploadResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
