package brain.api.tts.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TtsClientMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class TtsModelDao {

    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public TtsModelDto getTtsModelDto(Map <String, String> ttsInfo){
        logger.debug(ttsInfo);
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsClientMapper mapper = session.getMapper(TtsClientMapper.class);
            return mapper.getTtsModelDto(ttsInfo);
        }
    }

    public TtsModelDto getPublicTtsModelDto(Map<String, String> modelInfo){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsClientMapper mapper = session.getMapper(TtsClientMapper.class);
            return mapper.getPublicTtsModelDto(modelInfo);
        }
    }

    public TtsModelDto getTtsModelDtoForAdmin(String model) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsClientMapper mapper = session.getMapper(TtsClientMapper.class);
            return mapper.getTtsModelDtoForAdmin(model);
        }
    }

    public TtsModelDto getOpenModel(Map<String, String> params){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsClientMapper mapper = session.getMapper(TtsClientMapper.class);
            return mapper.getOpenModel(params);
        }
    }

}
