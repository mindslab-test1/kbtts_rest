package brain.api.edueng.model;

import brain.api.edueng.EduengCommonCode;

public enum EduengApiCallType {
    STT(EduengCommonCode.STT_SUB_URL, EduengCommonCode.STT_SERVICE_NAME),
    PRON(EduengCommonCode.PRON_SUB_URL, EduengCommonCode.PRON_SERVICE_NAME),
    PHONICS(EduengCommonCode.PHONICS_SUB_URL, EduengCommonCode.PHONICS_SERVICE_NAME);

    private final String subUrl;
    private final String serviceName;

    EduengApiCallType(String subUrl, String serviceName) {
        this.subUrl = subUrl;
        this.serviceName = serviceName;
    }

    public String getSubUrl() {
        return subUrl;
    }

    public String getServiceName() {
        return serviceName;
    }
}
