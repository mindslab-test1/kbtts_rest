package brain.api.qa.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.qa.QaCommonCode;
import brain.api.qa.data.dto.QuestionDto;
import brain.api.qa.service.QaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/qa")
public class QaController {
    private static final CloudApiLogger logger = new CloudApiLogger(QaCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final QaService service;

    @PostMapping(
            value = "/answer",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE // set response data encoding utf-8
    )
    public ResponseEntity<?> answer(@Validated @RequestBody QuestionDto dto, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new LinkedHashMap<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                errors.put(error.getField(), error.getDefaultMessage());
            }
            ErrorResponse errorResponse = new ErrorResponse(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "invalid parameter"
            );
            errorResponse.setData(errors);

            return new ResponseEntity<>(
                    errorResponse,
                    HttpStatus.BAD_REQUEST
            );
        }
        logger.debug(dto.toString());
        try {
            return ResponseEntity.ok(service.getAnswersByQuestion(dto, request));
        } catch (MindsRestException mre) {
            logger.error(mre.getMessage());
            return new ResponseEntity<>(new ErrorResponse(mre), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
