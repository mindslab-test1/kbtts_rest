package brain.api.classification.client;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.data.ImageClassificationResponse;
import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.image_classification.Classification;
import maum.brain.image_classification.ImageClsGrpc;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class TripleEClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final Map<String, TripleEClient.ChannelHolder> channelMap = new HashMap<>();

    private ImageClsGrpc.ImageClsStub imageClsStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();
            String channelKey = String.format("set destination %s, %d", ip, port);
            logger.debug(channelKey);
            ManagedChannel channel = CommonUtils.getManagedChannel(ip, port);
            this.imageClsStub = ImageClsGrpc.newStub(
                    channel
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            channelMap.put(channelKey, new ChannelHolder(
                    channel, System.currentTimeMillis()
            ));
            return new CommonMsg();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        } finally {
            this.manageChannel();
        }
    }

    public ImageClassificationResponse classifyImage(byte[] imageBytes) throws MindsRestException {
        ManagedChannel channel;
        String channelKey = String.format("set destination %s, %d", super.destIp, super.destPort);
        logger.debug(channelKey);
        if (channelMap.containsKey(channelKey)) {
            channel = channelMap.get(channelKey).channel;
            channelMap.get(channelKey).lastReferencedEpoch = System.currentTimeMillis();
        } else {
            channel = CommonUtils.getManagedChannel(super.destIp, super.destPort);
            channelMap.put(channelKey, new ChannelHolder(
                    channel, System.currentTimeMillis()
            ));
        }

        final CountDownLatch finishLatch = new CountDownLatch(1);

        ImageClassificationResponse imageClassificationResponse = new ImageClassificationResponse();

        StreamObserver<Classification.Input> requestObserver = imageClsStub.determinate(new StreamObserver<Classification.ClsResult>() {
            @Override
            public void onNext(Classification.ClsResult clsResult) {
                imageClassificationResponse.setResult(clsResult.getName());
                imageClassificationResponse.setCode(clsResult.getClassId());
                logger.debug(imageClassificationResponse);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Error: Stream Response onError from streamClassification");
                logger.error(throwable.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Stream Response from streamClassification");
                finishLatch.countDown();
            }
        });

        // Stream Request (Image)
        Classification.Input.Builder builder = Classification.Input.newBuilder();

        // try - catch - resources 방식 (autoClosable 구현체라 inputStream을 안닫아도 됨.
        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes)) {
            int len;
            byte[] buffer = new byte[1024 * 512];
            while ((len = bis.read(buffer)) > 0) {
                ByteString inputByteString;
                if (buffer.length > len) { // 파일 마지막의 크기가 기준보다 작으면
                    inputByteString = ByteString.copyFrom(Arrays.copyOfRange(buffer, 0, len - 1));
                } else {
                    inputByteString = ByteString.copyFrom(buffer);
                }
                requestObserver.onNext(builder.setImage(inputByteString).build());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
        requestObserver.onCompleted();

        boolean timeout;

        // 2. Waiting till async work is done
        try {
            timeout = finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e);
        }

        if (!timeout) {
            logger.error("gRPC async request timeout");
            throw new MindsRestException();
        }

        logger.info(imageClassificationResponse);

        return imageClassificationResponse;
    }

    private static class ChannelHolder {
        public ManagedChannel channel;
        public long lastReferencedEpoch;

        ChannelHolder(ManagedChannel channel, long lastReferencedEpoch) {
            this.channel = channel;
            this.lastReferencedEpoch = lastReferencedEpoch;
        }
    }

    private void manageChannel() {
        logger.debug("manage channel");
        if (channelMap.size() > 10) {
            logger.debug("maintained channel over 10");
            String channelKey = "";
            long latestReferencedTime = System.currentTimeMillis();
            for (Map.Entry<String, TripleEClient.ChannelHolder> entry : channelMap.entrySet()) {
                if (entry.getValue().lastReferencedEpoch < latestReferencedTime) {
                    channelKey = entry.getKey();
                    latestReferencedTime = entry.getValue().lastReferencedEpoch;
                }
            }
            ManagedChannel removeTargetChannel = channelMap.get(channelKey).channel;
            channelMap.remove(channelKey);
            removeTargetChannel.shutdown();
            logger.debug(String.format("removed %s", channelKey));
        }
        logger.debug(String.format("channel map size %d", channelMap.size()));
    }

    @PreDestroy
    private void onDestroy() {
        for (Map.Entry<String, TripleEClient.ChannelHolder> entry : channelMap.entrySet()) {
            entry.getValue().channel.shutdown();
        }
    }

}
