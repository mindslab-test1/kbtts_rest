package brain.api.common.data;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseUploadedFileInfo {
    private String filename;
    private String fileDownloadUri;
    private String fileType;
    private long fileSize;
}
