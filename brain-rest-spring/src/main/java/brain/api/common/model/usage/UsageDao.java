package brain.api.common.model.usage;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.UsageMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UsageDao {
    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private final SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int updateUsage(UsageDto dto){
        try (SqlSession session = sessionFactory.openSession(true)){
            UsageMapper mapper = session.getMapper(UsageMapper.class);
            return mapper.updateUsage(dto);
        }
    }
    public List<SumUsageDto> sumUsageDto(SumUsageRequestParam param) {
        try (SqlSession session = sessionFactory.openSession(true)){
            UsageMapper mapper = session.getMapper(UsageMapper.class);
            return mapper.selectSumUsage(param);
        }
    }
}
