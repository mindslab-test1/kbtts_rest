package brain.api.tts.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TtsUsageMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TtsUsageDao {
    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private final SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int insertTtsUsage(TtsUsageDto usageDto){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsUsageMapper mapper = session.getMapper(TtsUsageMapper.class);
            return mapper.insertTtsUsage(usageDto);
        }
    }
    public List<TtsUsageDto> selectAll(){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsUsageMapper mapper = session.getMapper(TtsUsageMapper.class);
            return mapper.selectAll();
        }
    }
    public List<TtsUsageDto> selectByDate(TtsUsageSelectDto usageSelectDto){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsUsageMapper mapper = session.getMapper(TtsUsageMapper.class);
            return mapper.selectByDate(usageSelectDto);
        }
    }

    public int insertTtsResult(TtsResultDto dto){
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsUsageMapper mapper = session.getMapper(TtsUsageMapper.class);
            return mapper.insertTtsResult(dto);
        }
    }
}
