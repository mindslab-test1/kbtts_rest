package brain.api.lipsync.v2.client;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.lipsync.LipSyncCommonCode;
import brain.api.lipsync.LipsyncKeyCollector;
import brain.api.lipsync.v1.data.LipSyncKey;
import brain.api.lipsync.v1.data.LipSyncStatus;
import brain.api.lipsync.v2.data.Resolution;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import maum.brain.lipsync.v2.BrainLipsync;
import maum.brain.lipsync.v2.LipSyncGenerationGrpc;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Component
public class LipSyncClient {
    private static final CloudApiLogger logger = new CloudApiLogger(LipSyncCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private final LipsyncKeyCollector lipsyncKeyCollector;
    private final Map<String, LipSyncClient.ChannelHolder> channelMap = new HashMap<>();

    public LipSyncKey upload(
            String text,
            byte[] imageBytes,
            Resolution resolution,
            boolean transparent,
            String host,
            int port,
            String version
    ) throws IOException, InterruptedException, MindsRestException {
        String channelKey = CommonUtils.createChannelKey(host, port);

        // TODO: Refactoring Target, Extract LipsyncChannelCollector
        ManagedChannel channel;
        if (channelMap.containsKey(channelKey)) {
            channel = channelMap.get(channelKey).channel;
            channelMap.get(channelKey).lastReferencedEpoch = System.currentTimeMillis();
        } else {
            channel = CommonUtils.getManagedChannel(host, port);
            channelMap.put(channelKey, new ChannelHolder(
                    channel, System.currentTimeMillis()
            ));
        }

        LipSyncGenerationGrpc.LipSyncGenerationStub stub = LipSyncGenerationGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        List<LipSyncKey> responseList = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<BrainLipsync.LipSyncRequestKey> responseObserver = new StreamObserver<BrainLipsync.LipSyncRequestKey>() {
            @Override
            public void onNext(BrainLipsync.LipSyncRequestKey lipSyncRequestKey) {
                responseList.add(new LipSyncKey(
                        lipSyncRequestKey.getRequestKey()
                ));
                logger.debug("request key received");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                errorList.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.debug("server on completed called");
                finishLatch.countDown();
            }
        };

        StreamObserver<BrainLipsync.LipSyncInput> requestObserver = stub.upload(responseObserver);
        BrainLipsync.LipSyncInput.Builder inputBuilder = BrainLipsync.LipSyncInput.newBuilder();
        requestObserver.onNext(inputBuilder
                .setText(text + LipSyncCommonCode.INFERENCE_DOT)
                .setResolution(BrainLipsync.VideoResolution.newBuilder()
                        .setWidth(resolution.getWidth())
                        .setHeight(resolution.getHeight())
                        .build())
                .setTransparent(transparent && imageBytes == null)
                .build());
        logger.debug("send text chunk to server");

        if (imageBytes != null) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBytes);
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while ((len = byteArrayInputStream.read(buffer)) > 0) {
                logger.debug(String.format("image chunk size: %d", len));
                if (len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(inputBuilder.setBackground(inputByteString).build());
                logger.debug("send image chunk to server");
            }
        }
        requestObserver.onCompleted();

        if (!finishLatch.await(1, TimeUnit.MINUTES)) {
            logger.error("connection timeout");
            throw new MindsRestException("connection timeout with internal server");
        }
        if (!errorList.isEmpty()) {
            logger.error(errorList.get(0));
            throw new MindsRestException();
        }

        LipSyncKey lipSyncKey = responseList.get(0);

        String fileExtension = lipsyncKeyCollector.getExtensionByTransparent(transparent, imageBytes);
        // TODO: Refactoring Target, index 하드코딩부분 D550-255 이슈에서 해결 필요
        lipsyncKeyCollector.createLipsyncKey(responseList.get(0).getRequestKey(), fileExtension, channelKey, version);

        return lipSyncKey;
    }

    public LipSyncStatus statusCheck(String requestKey) throws MindsRestException {
        String channelAddress = lipsyncKeyCollector.getByRequestKey(requestKey).getChannelKey();
        if (channelAddress == null) throw new MindsRestException(
                CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "request key is invalid"
        );

        LipSyncGenerationGrpc.LipSyncGenerationBlockingStub stub = null;

        // TODO: Refactoring Target, Extract LipsyncChannelCollector
        if (channelMap.get(channelAddress) != null) {
            stub = LipSyncGenerationGrpc.newBlockingStub(channelMap.get(channelAddress).channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        } else {
            stub = LipSyncGenerationGrpc.newBlockingStub(new ChannelHolder(
                    CommonUtils.getManagedChannel(channelAddress.split(":")[0], Integer.parseInt(channelAddress.split(":")[1])), System.currentTimeMillis()).channel)
                    .withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        }

        BrainLipsync.StatusMsg statusMessage = stub.statusCheck(BrainLipsync.LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build());

        logger.debug(statusMessage.getStatusCode());
        logger.debug(statusMessage.getMsg());
        logger.debug(statusMessage.getWaiting());

        return new LipSyncStatus(
                statusMessage.getStatusCode().getNumber(),
                statusMessage.getMsg(),
                statusMessage.getWaiting()
        );
    }

    public ByteArrayOutputStream download(String requestKey) throws MindsRestException {
        String channelAddress = lipsyncKeyCollector.getByRequestKey(requestKey).getChannelKey();
        if (channelAddress == null) throw new MindsRestException(
                CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "request key is invalid"
        );

        LipSyncGenerationGrpc.LipSyncGenerationBlockingStub stub = null;

        // TODO: Refactoring Target, Extract LipsyncChannelCollector
        if (channelMap.get(channelAddress) != null) {
            stub = LipSyncGenerationGrpc.newBlockingStub(channelMap.get(channelAddress).channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        } else {
            stub = LipSyncGenerationGrpc.newBlockingStub(new ChannelHolder(
                    CommonUtils.getManagedChannel(channelAddress.split(":")[0], Integer.parseInt(channelAddress.split(":")[1])), System.currentTimeMillis()).channel)
                    .withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        }

        BrainLipsync.StatusMsg statusMsg = stub.statusCheck(BrainLipsync.LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build());
        if (statusMsg.getStatusCodeValue() != 2) {
            logger.warn(statusMsg.getStatusCode());
            logger.warn(statusMsg.getMsg());
            throw new MindsRestException(
                    2002,
                    "Process is not done"
            );
        }

        Iterator<BrainLipsync.LipSyncResult> rawIterator = stub.download(BrainLipsync.LipSyncRequestKey.newBuilder()
                .setRequestKey(requestKey)
                .build()
        );

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            for (int i = 1; rawIterator.hasNext(); i++) {
                BrainLipsync.LipSyncResult response = rawIterator.next();
                outputStream.write(response.getVideo().toByteArray());
            }
        } catch (Exception e) {
            logger.info("Error: Stream response from Avatar downloadAvatar");
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }

        return outputStream;
    }

    private class ChannelHolder {
        public ManagedChannel channel;
        public long lastReferencedEpoch;

        ChannelHolder(ManagedChannel channel, long lastReferencedEpoch) {
            this.channel = channel;
            this.lastReferencedEpoch = lastReferencedEpoch;
        }
    }

    private void manageChannel() {
        logger.debug("manage channel");
        if (channelMap.size() > 10) {
            logger.debug("maintained channel over 10");
            String channelKey = "";
            long latestReferencedTime = System.currentTimeMillis();
            for (Map.Entry<String, ChannelHolder> entry : channelMap.entrySet()) {
                if (entry.getValue().lastReferencedEpoch < latestReferencedTime) {
                    channelKey = entry.getKey();
                    latestReferencedTime = entry.getValue().lastReferencedEpoch;
                }
            }
            ManagedChannel removeTargetChannel = channelMap.get(channelKey).channel;
            channelMap.remove(channelKey);
            removeTargetChannel.shutdown();
            logger.debug(String.format("removed %s", channelKey));
        }
        logger.debug(String.format("channel map size %d", channelMap.size()));
    }

    @PreDestroy
    private void onDestroy() {
        for (Map.Entry<String, ChannelHolder> entry : channelMap.entrySet()) {
            entry.getValue().channel.shutdown();
        }
    }
}
