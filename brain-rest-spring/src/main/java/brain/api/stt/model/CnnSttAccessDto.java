package brain.api.stt.model;

public class CnnSttAccessDto {
    private String apiId;
    private String lang;
    private String sttModel;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getSttModel() {
        return sttModel;
    }
    public void setSttModel(String sttModel) {
        this.sttModel = sttModel;
    }

    @Override
    public String toString() {
        return "CnnSttAccessDto{" +
                "apiId='" + apiId + '\'' +
                ", lang='" + lang + '\'' +
                ", sttModel='" + sttModel + '\'' +
                '}';
    }
}
