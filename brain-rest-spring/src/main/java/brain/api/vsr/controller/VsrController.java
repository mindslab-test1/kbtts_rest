package brain.api.vsr.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.vsr.service.VsrService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("/vsr")
public class VsrController {

    @Autowired
    VsrService vsrService;

    @RequestMapping(
            value = "/download",
            method = RequestMethod.POST
    ) public ResponseEntity downloadVsr(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file")MultipartFile file,
            HttpServletRequest httpRequest
    ){
        String orgFilename = file.getOriginalFilename();
        String filename = FilenameUtils.getBaseName(orgFilename);
        String extension = FilenameUtils.getExtension(orgFilename);
        if(file.isEmpty()){
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "File is empty"));
        }
        else if(extension.equals("mp4") && extension.equals("avi") && extension.equals("mkv")){
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Support type: mp4, avi, mkv"));
        }
        try {
            ByteArrayOutputStream buffer = vsrService.downloadVsr(apiId,apiKey,file,httpRequest);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=vsr-"+filename+'.'+extension)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (Exception e) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    e.getMessage()));
        }
    }
}
