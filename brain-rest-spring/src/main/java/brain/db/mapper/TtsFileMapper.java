package brain.db.mapper;

import brain.api.tts.model.server.TtsFileDownloadDto;
import brain.api.tts.model.server.TtsFileInfoDto;
import brain.api.tts.model.server.TtsModelDto;

import java.util.List;
import java.util.Map;

public interface TtsFileMapper {
    int insertFileInfo(TtsFileInfoDto fileDto);
    int insertFileDownload(TtsFileDownloadDto downloadDto);
    List<String> selectRemoveFileList(String date);
    int updateDeletedFileStatus(String fileId);
}
