package brain.api.avatar.controller;

import brain.api.avatar.service.AvatarService;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.util.List;

@RestController
@RequestMapping("/avatar")
public class AvatarController {

    @Autowired
    AvatarService avatarService;

    @RequestMapping(
        value = "/download",
        method = RequestMethod.POST
    ) public ResponseEntity downloadAvatar(
        @RequestParam("apiId") String apiId,
        @RequestParam("apiKey") String apiKey,
        @RequestParam("images") List<MultipartFile> images,
        @RequestParam("video") MultipartFile video,
        HttpServletRequest httpRequest
    ){
        if(images.size() > 5){
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    "Images have to less then 6"));
        }
        try {
            ByteArrayOutputStream buffer = avatarService.downloadAvatar(apiId, apiKey, images,video, httpRequest);
            String orgFilename = video.getOriginalFilename();
            String filename = orgFilename.substring(0, orgFilename.lastIndexOf('.'));
            String extension = ".mp4";
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=avatar-"+filename+extension)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (Exception e) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    e.getMessage()));
        }
    }
}
