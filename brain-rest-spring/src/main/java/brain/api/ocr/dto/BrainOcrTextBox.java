package brain.api.ocr.dto;

import java.util.List;

public class BrainOcrTextBox {
    private String text;
    private List<BrainOcrPoint> bbox;

    public BrainOcrTextBox() {
    }

    public BrainOcrTextBox(String text, List<BrainOcrPoint> points) {
        this.text = text;
        this.bbox = points;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<BrainOcrPoint> getBbox() {
        return bbox;
    }

    public void setBbox(List<BrainOcrPoint> bbox) {
        this.bbox = bbox;
    }

    @Override
    public String toString() {
        return "BrainOcrTextBox{" +
                "text='" + text + '\'' +
                ", bbox=" + bbox +
                '}';
    }
}
