package brain.api.konglish.model;

import java.util.List;

public class KonglishReqSentenceParams {
    private String apiId;
    private String apiKey;
    private String sentence;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSentence() {
        return sentence;
    }
    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public String toString() {
        return "KonglishReqSentenceParams{" +
                "apiId=" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", sentence='" + sentence + '\'' +
                '}';
    }
}
