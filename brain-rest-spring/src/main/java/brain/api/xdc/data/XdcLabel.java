package brain.api.xdc.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcLabel implements Serializable {
    private String label;
    private double probability;

    public XdcLabel(String label, double probability) {
        this.label = label;
        this.probability = probability;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "XdcLabel{" +
                "label='" + label + '\'' +
                ", probability=" + probability +
                '}';
    }
}
