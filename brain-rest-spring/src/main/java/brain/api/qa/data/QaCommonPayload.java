package brain.api.qa.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public abstract class QaCommonPayload {
    private String answer;
}
