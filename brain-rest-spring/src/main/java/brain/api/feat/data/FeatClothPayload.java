package brain.api.feat.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FeatClothPayload implements Serializable {
    private List<FeatClothPayloadMeta> metaData;
    private byte[] resultImage;

    public FeatClothPayload() {
    }

    public FeatClothPayload(byte[] resultImage, List<FeatClothPayloadMeta> metaData) {
        this.resultImage = resultImage;
        this.metaData = metaData;
    }

    public byte[] getResultImage() {
        return resultImage;
    }

    public void setResultImage(byte[] resultImage) {
        this.resultImage = resultImage;
    }

    public List<FeatClothPayloadMeta> getMetaData() {
        return metaData;
    }

    public void setMetaData(List<FeatClothPayloadMeta> metaData) {
        this.metaData = metaData;
    }

    @Override
    public String toString() {
        return "FeatClothPayload{" +
                "resultImage=" + Arrays.toString(resultImage) +
                ", metaData=" + metaData +
                '}';
    }
}
