package brain.api.dap.data.recog;

import brain.api.common.data.CommonMsg;
import brain.api.dap.data.DapBaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SetVoiceResponse extends DapBaseResponse {
    public SetVoiceResponse(CommonMsg message) {super(message);}
}
