package brain.api.dap.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DapDiarizeSpeaker implements Serializable {
    private int speaker;
    private double startTime;
    private double endTime;

    public DapDiarizeSpeaker() {
    }

    public DapDiarizeSpeaker(int speaker, double startTime, double endTime) {
        this.speaker = speaker;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getSpeaker() {
        return speaker;
    }

    public void setSpeaker(int speaker) {
        this.speaker = speaker;
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "DapDiarizeSpeaker{" +
                "speaker=" + speaker +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
