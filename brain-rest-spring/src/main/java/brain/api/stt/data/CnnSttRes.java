package brain.api.stt.data;

import brain.api.common.data.CommonMsg;

public class CnnSttRes {
    private CommonMsg commonMsg;
    private String result;

    public CnnSttRes(String result) {
        this.commonMsg = new CommonMsg();
        this.result = result;
    }

    public CnnSttRes(CommonMsg commonMsg) {
        this.commonMsg = commonMsg;
        this.result = null;
    }

    public CommonMsg getCommonMsg() {
        return commonMsg;
    }
    public void setCommonMsg(CommonMsg commonMsg) {
        this.commonMsg = commonMsg;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CnnSttRes{" +
                "commonMsg=" + commonMsg +
                ", result='" + result + '\'' +
                '}';
    }
}
