package brain.api.konglish.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.konglish.KonglishCommonCode;
import brain.api.konglish.data.KonglishResWords;
import maum.brain.konglish.KonglishGrpc;
import maum.brain.konglish.KonglishOuterClass;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class KonglishClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(KonglishCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private KonglishGrpc.KonglishBlockingStub konglishStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            konglishStub = KonglishGrpc.newBlockingStub(
                CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

            return new CommonMsg();
        } catch (Exception e){
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public KonglishResWords doKonglishWords(List<String> wordList) throws MindsRestException {
        logger.info("[doKonglishWords] - " + wordList);
        try {
            KonglishOuterClass.English inputText = KonglishOuterClass.English.newBuilder()
                    .addAllWords(wordList)
                    .build();
            KonglishOuterClass.Korean output = this.konglishStub.transliterate(inputText);
            logger.info("[doKonglishWords] - " + output.getWordsList());

            KonglishResWords response = new KonglishResWords(output.getWordsList());
            return response;
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    public String doKonglishSentence(String sentence) throws  MindsRestException {
        logger.info("[doKonglishSentence] - " + sentence);
        try {
            List<String> wordList = new ArrayList<>();

            // Regex => Sentence to wordList
            Pattern regex = Pattern.compile("([a-zA-Z]+'[a-zA-Z]+)|[a-zA-Z]+");
            Matcher matcher = regex.matcher(sentence);
            while(matcher.find()){
                wordList.add(matcher.group());
            }
            logger.debug(wordList);
            KonglishOuterClass.English inputText = KonglishOuterClass.English.newBuilder()
                    .addAllWords(wordList)
                    .build();
            KonglishOuterClass.Korean outputs = this.konglishStub.transliterate(inputText);

            String response = sentence;
            logger.debug(outputs.getWordsList());
            if(wordList.size() > 0){
                for(int i = 0; i < wordList.size(); i++){
                    String engWord = wordList.get(i);
                    String korWord = outputs.getWordsList().get(i);
                    response = response.replaceFirst(engWord, korWord);
                }
            }
            logger.debug(response);

            return response;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }
}
