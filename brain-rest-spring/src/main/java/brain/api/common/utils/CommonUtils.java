package brain.api.common.utils;

import brain.api.common.CommonCode;
import brain.api.common.async.grpc.GrpcMDCThreadPoolExecutor;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.interceptor.grpc.GrpcLoggingInterceptor;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.drama.RabbitMqDto;
import brain.api.nlp.data.*;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.nlp.Nlp;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Component
public class CommonUtils {
    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_UTILS);

    // TODO: Spring Bean으로 싱글톤화할시 MDC sharing이 안되는 이슈가 있음.
    public static ThreadPoolExecutor gRPCThreadPoolExecutor() {
        return GrpcMDCThreadPoolExecutor.newWithCurrentMdc(
                Runtime.getRuntime().availableProcessors(),
                Runtime.getRuntime().availableProcessors() * 5,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(5)
        );
    }

    public static ManagedChannel getManagedChannel(String ip, int port){
        return ManagedChannelBuilder.forAddress(ip, port)
                .usePlaintext()
                .intercept(new GrpcLoggingInterceptor())
                .directExecutor()
                .keepAliveWithoutCalls(true)
                .build();
    }

    public static String generateRequestUniqueKey(boolean withoutSeparator) {
        if (withoutSeparator) {
            try {
                return MDC.get(CommonCode.TRACE_ID_KEY).replaceAll("-", "");
            } catch (IllegalArgumentException e) {
                logger.warn(e.getLocalizedMessage());
                return UUID.randomUUID().toString().replace("-", "");
            }
        } else {
            try {
                return MDC.get(CommonCode.TRACE_ID_KEY);
            } catch (IllegalArgumentException e) {
                logger.warn(e.getLocalizedMessage());
                return UUID.randomUUID().toString();
            }
        }
    }

    public static String createChannelKey(String host, int port) {
        return String.format("%s:%d", host, port);
    }

    public static String dramaBuildMessageKey(RabbitMqDto dto) throws NoSuchAlgorithmException {
        final String NAMESPACE = "dramatiq-results";
        String buildText = NAMESPACE + ":" + dto.getQueue_name() + ":" + dto.getActor_name() + ":" + dto.getMessage_id();
        String messageKey;
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(buildText.getBytes());
        BigInteger hash = new BigInteger(1, md5.digest());
        messageKey = hash.toString(16);
        while(messageKey.length() < 32) {messageKey = "0" + messageKey;}

        return messageKey;
    }

    public static NlpDocument nlpDocumentBuilder(Nlp.Document rawDoc){

        ArrayList<NlpSentence> sentences = new ArrayList<>();
        for(Nlp.Sentence sentence: rawDoc.getSentencesList()) {
            ArrayList<NlpWord> words = new ArrayList<>();
            for(Nlp.Word word: sentence.getWordsList())
                words.add(
                        new NlpWord(
                                word.getSeq(),
                                word.getText(),
                                word.getTaggedText(),
                                word.getType(),
                                word.getBegin(),
                                word.getEnd(),
                                word.getBeginSid(),
                                word.getEndSid(),
                                word.getPosition()
                        )
                );

            ArrayList<NlpMorpheme> morphemes = new ArrayList<>();
            for(Nlp.Morpheme morpheme: sentence.getMorpsList())
                morphemes.add(
                        new NlpMorpheme(
                                morpheme.getSeq(),
                                morpheme.getLemma(),
                                morpheme.getType(),
                                morpheme.getPosition(),
                                morpheme.getWid(),
                                morpheme.getWeight()
                        )
                );

            ArrayList<NlpMorphemeEval> morphemeEvals = new ArrayList<>();
            for(Nlp.MorphemeEval morphemeEval: sentence.getMorphEvalsList())
                morphemeEvals.add(
                        new NlpMorphemeEval(
                                morphemeEval.getSeq(),
                                morphemeEval.getTarget(),
                                morphemeEval.getResult(),
                                morphemeEval.getWordId(),
                                morphemeEval.getMBegin(),
                                morphemeEval.getMEnd()
                        )
                );

            ArrayList<NlpNamedEntity> namedEntities = new ArrayList<>();
            for(Nlp.NamedEntity namedEntity: sentence.getNesList())
                namedEntities.add(
                        new NlpNamedEntity(
                                namedEntity.getSeq(),
                                namedEntity.getText(),
                                namedEntity.getType(),
                                namedEntity.getBegin(),
                                namedEntity.getEnd(),
                                namedEntity.getTypeName(),
                                namedEntity.getWeight(),
                                namedEntity.getCommonNoun(),
                                namedEntity.getBeginSid(),
                                namedEntity.getEndSid()
                        )
                );

            sentences.add(
                    new NlpSentence(
                            sentence.getSeq(),
                            sentence.getText(),
                            words,
                            morphemes,
                            morphemeEvals,
                            namedEntities
                    )
            );
        }

        ArrayList<NlpKeywordFrequency> keywordFrequencies = new ArrayList<>();
        for(Nlp.KeywordFrequency keywordFrequency: rawDoc.getKeywordFrequenciesList())
            keywordFrequencies.add(
                    new NlpKeywordFrequency(
                            keywordFrequency.getSeq(),
                            keywordFrequency.getKeyword(),
                            keywordFrequency.getFrequency(),
                            keywordFrequency.getWordType(),
                            keywordFrequency.getWordTypeNm()
                    )
            );

        return new NlpDocument(
                rawDoc.getCategory(),
                rawDoc.getCategoryWeight(),
                sentences,
                keywordFrequencies
        );
    }

    public static HttpResponse sendPostRequest(HttpClient client, HttpPost post, HttpEntity entity) throws MindsRestException{
        logger.debug("send post request");
        try {
            post.setEntity(entity);
            return client.execute(post);
        } catch (IOException e){
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

    public static Map <String, String> parsingJson(HttpServletRequest request) throws IOException{
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();
        Gson gson = new Gson();

        inputStream = request.getInputStream();
        if (inputStream != null) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            char[] charBuffer = new char[128];
            int byteRead;
            while ((byteRead = bufferedReader.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, byteRead);
            }
        }

        if (bufferedReader != null) {
            bufferedReader.close();
        }

        String body = stringBuilder.toString();
        Type type = new TypeToken <Map<String, String>>(){}.getType();
        return gson.fromJson(body, type);
    }

    public static String getIp(HttpServletRequest request) {

        String uip = request.getHeader("X-FORWARDED-FOR");

        if (uip == null) {
            uip = request.getHeader("Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (uip == null) {
            uip = request.getRemoteAddr();
        }

        return uip;
    }

    public static String getDomain(HttpServletRequest request) {
        return request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
    }

    public static byte[] getBytes(MultipartFile file, int rate) throws IOException {
        File tempDir = Files.createTempDir();
        String tempFilePath = tempDir.getPath() + "/" + file.getOriginalFilename();
        File path = new File(tempFilePath);
        String extension = tempFilePath.substring(tempFilePath.lastIndexOf("."), tempFilePath.length());

        byte[] bArray;
        file.transferTo(path);

        String outputPath = tempDir.getPath() + "/output.pcm";

        String command = "";

        command = "ffmpeg -i " + tempFilePath + " -acodec pcm_s16le -f s16le -ar " + rate + " " + outputPath;

        Process processor = Runtime.getRuntime().exec(command);
        try {
            processor.waitFor();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        File fileIn = new File(outputPath);
        bArray = new byte[(int) fileIn.length()];

        FileInputStream pcm = new FileInputStream(fileIn);
        BufferedInputStream bis = new BufferedInputStream(pcm);
        bis.read(bArray);
        bis.close();

        FileUtils.cleanDirectory(tempDir);
        tempDir.delete();
        return bArray;
    }

    public static String readableFileSizeUnit(long size) throws MindsRestException {
        if (size < 0) throw new MindsRestException("Only 0 or positive numbers are allowed.");
        if (size >= FileUtils.ONE_PB) throw new MindsRestException("It only supports up to TB unit mark.");
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(FileUtils.ONE_KB));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(FileUtils.ONE_KB, digitGroups)) + " " + units[digitGroups];
    }

    public static boolean validateRegex(String pattern, String input) {
        return Pattern.matches(pattern, input);
    }
}
