package brain.api.lipsync.v1.data;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public enum LipsyncStatusCode {
    NOT_YET,
    PROCESSING,
    DONE,
    WRONG_KEY,
    ERROR,
    DELETED;

    private static final Map<Integer, LipsyncStatusCode> LIPSYNC_STATUS_CODE_MAP = Stream.of(values()).collect(toMap(e -> e.ordinal(), e -> e));

    public static LipsyncStatusCode fromOrdinal(int value) {
        return LIPSYNC_STATUS_CODE_MAP.get(value);
    }
}
