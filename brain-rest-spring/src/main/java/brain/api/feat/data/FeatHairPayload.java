package brain.api.feat.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Arrays;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FeatHairPayload implements Serializable {
    private byte[] resultImage;
    private String hairInfo;

    public FeatHairPayload() {
    }

    public FeatHairPayload(byte[] resultImage, String hairInfo) {
        this.resultImage = resultImage;
        this.hairInfo = hairInfo;
    }

    public byte[] getResultImage() {
        return resultImage;
    }

    public void setResultImage(byte[] resultImage) {
        this.resultImage = resultImage;
    }

    public String getHairInfo() {
        return hairInfo;
    }

    public void setHairInfo(String hairInfo) {
        this.hairInfo = hairInfo;
    }

    @Override
    public String toString() {
        return "FeatHairPayload{" +
                "resultImage=" + Arrays.toString(resultImage) +
                ", hairInfo='" + hairInfo + '\'' +
                '}';
    }
}
