package brain.api.common.model.user;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.usage.UsageDto;
import brain.db.common.BrainRestSqlConfig;
import brain.db.common.DjangoSqlConfig;
import brain.db.mapper.ApiClientMapper;
import brain.db.mapper.DjangoMapper;
import brain.db.mapper.UsageMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ApiClientDao {
    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory brainRestSessionFactory = BrainRestSqlConfig.getSessionFactory();
    private SqlSessionFactory djangoSessionFactory = DjangoSqlConfig.getSessionFactory();

    public ApiClientDto getApiClientInfo(String apiId){
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            ApiClientMapper mapper = session.getMapper(ApiClientMapper.class);
            return mapper.getApiClientInfo(apiId);
        }
    }

    public List<ApiClientDto> getApiClientList() {
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            ApiClientMapper mapper = session.getMapper(ApiClientMapper.class);
            return mapper.getApiClientList();
        }
    }

    public int setNewApiClient(ApiClientDto dto){
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            ApiClientMapper mapper = session.getMapper(ApiClientMapper.class);
            return mapper.setNewApiClient(dto);
        }
    }

    public List<UsageDto> selectUsage(Map reqParam) {
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            UsageMapper mapper = session.getMapper(UsageMapper.class);
            return mapper.selectUsage(reqParam);
        }
    }

    public int insertDjangoClient(DjangoClientDto clientDto) {
        try (SqlSession session = djangoSessionFactory.openSession(true)){
            DjangoMapper mapper = session.getMapper(DjangoMapper.class);
            return mapper.insertDjangoClient(clientDto);
        }
    }

    public List<UsageDto> selectSttusage(Map reqParam) {
        try (SqlSession session = djangoSessionFactory.openSession(true)){
            DjangoMapper mapper = session.getMapper(DjangoMapper.class);
            return mapper.selectSttusage(reqParam);
        }
    }

    public int updateClient(ApiClientDto apiClientDto) {
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            ApiClientMapper mapper = session.getMapper(ApiClientMapper.class);
            return mapper.updateClient(apiClientDto);
        }
    }

    public int updateClientKey(ApiClientDto apiClientDto) {
        try (SqlSession session = brainRestSessionFactory.openSession(true)){
            ApiClientMapper mapper = session.getMapper(ApiClientMapper.class);
            return mapper.updateClientKey(apiClientDto);
        }
    }
}
