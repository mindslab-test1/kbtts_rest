package brain.api.esr.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.esr.EsrCommonCode;
import com.google.protobuf.ByteString;
import maum.brain.esr.EnhancedSuperResolutionGrpc;
import maum.brain.esr.Esr;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class EsrGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(EsrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private EnhancedSuperResolutionGrpc.EnhancedSuperResolutionBlockingStub esrStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip ,port));
            this.esrStub = EnhancedSuperResolutionGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public byte[] superResolutaion(MultipartFile img) throws MindsRestException {
        try {
            ByteString imgByteString = ByteString.copyFrom(
                    img.getBytes()
            );
            Esr.InputImg inputImg = Esr.InputImg.newBuilder().setImgBytes(imgByteString).build();
            return esrStub.superResolution(inputImg).getImgBytes().toByteArray();

        } catch (IOException e) {
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

}
