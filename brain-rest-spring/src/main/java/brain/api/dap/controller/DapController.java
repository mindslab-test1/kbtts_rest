package brain.api.dap.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.log.CloudApiLogger;
import brain.api.dap.DapCommonCode;
import brain.api.dap.service.DapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/dap")
public class DapController {
    private static final CloudApiLogger logger = new CloudApiLogger(DapCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    DapService service;

    @RequestMapping(
            value = "/app/setVoice",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appSetVoice(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("voiceId") String voiceId
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try{
            logger.info("[SetVoice invoked] apiId : " + apiId + ", dbId : " + dbId + ", voiceId : " + voiceId);
            return new ResponseEntity(service.setVoice(apiId, apiKey, dbId, voiceId, file, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/getVoiceList",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appGetVoiceList(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , HttpServletRequest request
    ) {
        try {
            logger.info("[GetVoiceList invoked] apiId : " + apiId + ", dbId : " + dbId);
            return new ResponseEntity(service.getVoiceList(apiId, apiKey, dbId, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/deleteVoice",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appDeleteVoice (
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("voiceId") String voiceId
            , HttpServletRequest request
    ) {
        logger.info("[DeleteVoice invoked] apiId : " + apiId + ", dbId : " + dbId);
        try {
            return new ResponseEntity(service.deleteVoice(apiId, apiKey, dbId, voiceId, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/recogVoice",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appRecogVoice (
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try {
            logger.info("[RecogVoice invoked] apiId : " + apiId + ", dbId : " + dbId);
            return new ResponseEntity(service.recogVoice(apiId, apiKey, dbId, file, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
