package brain.api.edueng.data;

import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import org.springframework.http.HttpStatus;

public class EduengRes {
    private CommonMsg commonMsg;
    private EduengResult result;

    public CommonMsg getCommonMsg() {
        return commonMsg;
    }

    public void setCommonMsg(CommonMsg commonMsg) {
        this.commonMsg = commonMsg;
    }

    public EduengResult getResult() {
        return result;
    }

    public void setResult(EduengResult result) {
        this.result = result;
    }

    public EduengRes(EduengResult result) {
        this.commonMsg = new CommonMsg();
        this.result = result;
    }

    public EduengRes(MindsRestException e) {
        this.commonMsg = new CommonMsg(e.getErrCode());
    }

    public EduengRes(Exception e) {
        this.commonMsg = new CommonMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
