package brain.api.common.model.user;

public class DjangoClientDto {
    private int id;
    private String name;
    private String email;
    private String clientID;
    private String clientKey;
    private boolean sttOn;
    private boolean nlaOn;
    private boolean mrcOn;
    private boolean usageLimit;

    public DjangoClientDto() {
    }

    public DjangoClientDto(String name, String email, String clientID, String clientKey) {
        this.name = name;
        this.email = email;
        this.clientID = clientID;
        this.clientKey = clientKey;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public boolean isSttOn() {
        return sttOn;
    }

    public void setSttOn(boolean sttOn) {
        this.sttOn = sttOn;
    }

    public boolean isNlaOn() {
        return nlaOn;
    }

    public void setNlaOn(boolean nlaOn) {
        this.nlaOn = nlaOn;
    }

    public boolean isMrcOn() {
        return mrcOn;
    }

    public void setMrcOn(boolean mrcOn) {
        this.mrcOn = mrcOn;
    }

    public boolean isUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(boolean usageLimit) {
        this.usageLimit = usageLimit;
    }

    @Override
    public String toString() {
        return "DjangoClientDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", clientID='" + clientID + '\'' +
                ", clientKey='" + clientKey + '\'' +
                ", sttOn=" + sttOn +
                ", nlaOn=" + nlaOn +
                ", mrcOn=" + mrcOn +
                ", usageLimit=" + usageLimit +
                '}';
    }
}
