package brain.api.style.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import maum.brain.stylebot_wild_retrieval.StylebotWildRetrieval;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserItemData implements Serializable {
    private String itemId;
    private int classId;

    public UserItemData() {
    }

    public UserItemData(StylebotWildRetrieval.UserItem userItem){
        this.itemId = userItem.getItemId();
        this.classId = userItem.getClassId();
    }

    public UserItemData(String itemId, int classId) {
        this.itemId = itemId;
        this.classId = classId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "UserItemData{" +
                "itemId='" + itemId + '\'' +
                ", classId=" + classId +
                '}';
    }
}
