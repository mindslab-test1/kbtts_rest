package brain.api.konglish.model;

import java.util.List;

public class KonglishReqWordsParams {
    private String apiId;
    private String apiKey;
    private List<String> wordList;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public List<String> getWordList() {
        return wordList;
    }
    public void setWordList(List<String> wordList) {
        this.wordList = wordList;
    }

    @Override
    public String toString() {
        return "KonglishReqWordsParams{" +
                "apiId=" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", wordList='" + wordList + '\'' +
                '}';
    }
}
