package brain.api.dap.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.dap.DapCommonCode;
import brain.api.dap.data.recog.*;
import com.google.gson.Gson;
import maum.brain.app.dap.RecogService;
import maum.brain.app.dap.VoiceRecogAppServiceGrpc;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class VoiceRecogServiceGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(DapCommonCode.SERVICE_APP_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private VoiceRecogAppServiceGrpc.VoiceRecogAppServiceBlockingStub voiceRecogAppServiceBlockingStub;
    private Gson gson = new Gson();

    public CommonMsg setDestination(String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.voiceRecogAppServiceBlockingStub = VoiceRecogAppServiceGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public SetVoiceResponse setVoice(String apiId, String dbId, String voiceId, List<Object> voiceVector) throws MindsRestException {
        try {
            List<Float> voiceVectorFloat = Object2Float(voiceVector);

            RecogService.SetVoiceRequest request = RecogService.SetVoiceRequest.newBuilder()
                    .setApiId(apiId)
                    .setDbId(dbId)
                    .setVoiceId(voiceId)
                    .addAllVoiceVector(voiceVectorFloat)
                    .build();

            RecogService.SetVoiceResponse result = voiceRecogAppServiceBlockingStub.setVoice(request);
            SetVoiceResponse response = new SetVoiceResponse(new CommonMsg(result.getStatus(), result.getMessage()));

            logger.info("[SetVoice Success Result] apiId : " + apiId + ", dbId : " + dbId + ", faceId : " + voiceId);

            return response;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    public GetVoiceResponse getVoiceList(String apiId, String dbId) throws MindsRestException {
        try {
            RecogService.GetVoiceRequest request = RecogService.GetVoiceRequest.newBuilder()
                    .setApiId(apiId)
                    .setDbId(dbId)
                    .build();

            RecogService.GetVoiceResponse result = voiceRecogAppServiceBlockingStub.getVoiceList(request);
            List<VoiceData> payLoad = new ArrayList<>();

            for (RecogService.VoiceData payLoadVal : result.getPayloadList()) {
                VoiceData voiceData = new VoiceData();
                voiceData.setId(payLoadVal.getId());

                List<Float> voiceVector = new ArrayList<>();
                for (float vector : payLoadVal.getVoiceVectorList()) {
                    voiceVector.add(vector);
                }
                voiceData.setVoiceVector(voiceVector);

                VoiceMetaData metaData = new VoiceMetaData();
                voiceData.setMetaData(metaData);
                setMetaData(metaData, payLoadVal.getMetadata());
                payLoad.add(voiceData);
            }

            GetVoiceResponse response = new GetVoiceResponse(new CommonMsg(), payLoad);

            logger.info("[GetVoiceList Success Result] apiId : " + apiId + ", dbId : " + dbId + ", ListSize : " + payLoad.size());

            return response;
        }catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    public DeleteVoiceResponse deleteVoice(String apiId, String dbId, String voiceId) throws MindsRestException {
        try {
            RecogService.DeleteVoiceRequest request = RecogService.DeleteVoiceRequest.newBuilder()
                    .setApiId(apiId)
                    .setDbId(dbId)
                    .setVoiceId(voiceId)
                    .build();
            RecogService.DeleteVoiceResponse result = voiceRecogAppServiceBlockingStub.deleteVoice(request);

            DeleteVoiceResponse response = new DeleteVoiceResponse(new CommonMsg(result.getStatus(), result.getMessage()));

            logger.info("[DeleteVoice Success Result] apiId : " + apiId + ", dbId : " + dbId + ", voiceId : " + voiceId);

            return response;
        } catch (Exception e) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    public VoiceRecogResponse recogVoice(String apiId, String dbId, List<Object> voiceVector) throws MindsRestException {
        try {
            List<Float> voiceVectorFloat = Object2Float(voiceVector);

            RecogService.VoiceRecogRequest request = RecogService.VoiceRecogRequest.newBuilder()
                    .setApiId(apiId)
                    .setDbId(dbId)
                    .addAllVoiceVector(voiceVectorFloat)
                    .build();

            RecogService.VoiceRecogResponse result = voiceRecogAppServiceBlockingStub.recogVoice(request);

            VoiceData voiceData = new VoiceData();
            voiceData.setId(result.getResult().getId());
            voiceData.setVoiceVector(result.getResult().getVoiceVectorList());

            VoiceMetaData metaData = new VoiceMetaData();
            setMetaData(metaData, result.getResult().getMetadata());
            voiceData.setMetaData(metaData);

            VoiceRecogResponse response = new VoiceRecogResponse(new CommonMsg(result.getStatus(), result.getMessage()), voiceData);

            logger.info("[RecogVoice Success Result] apiId : " + apiId + ", dbId : " + dbId + ", result : " + result.getMessage());

            return response;
        } catch (Exception e) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }
    }

    private VoiceMetaData setMetaData(VoiceMetaData voiceMetaData, RecogService.MetaData metaData){
        voiceMetaData.setUpdateTime(new Date(metaData.getUpdateTime().getSeconds()*1000));
        voiceMetaData.setCreateTime(new Date(metaData.getCreateTime().getSeconds()*1000));

        return voiceMetaData;
    }

    private List<Float> Object2Float(List<Object> voiceVector){
        List<Float> voiceVectorFloat = new ArrayList<>();
        for(Object i : voiceVector){
            voiceVectorFloat.add(((Double)i).floatValue());
        }
        return voiceVectorFloat;
    }
}
