package brain.api.d2a.controller;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.d2a.D2ACommonCode;
import brain.api.d2a.dto.ArticleResponse;
import brain.api.d2a.dto.WithPrecedentDto;
import brain.api.d2a.dto.WithTableDto;
import brain.api.d2a.service.D2AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(
        "/d2a"
)
public class D2AController {
    private static final CloudApiLogger logger = new CloudApiLogger(D2ACommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final D2AService service;

    public D2AController(@Autowired D2AService service){
        this.service = service;
    }

    @PostMapping(
            value = "/withPrecedent",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity withPrecedent(@RequestBody WithPrecedentDto dto, HttpServletRequest request){
        logger.info("with precedent request");
        try {
            ArticleResponse body = service.withPrecedent(dto, request);
            return ResponseEntity.ok(body);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).build();
        } catch (Exception e){
            logger.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "/withTable",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity withTable(@RequestBody WithTableDto dto, HttpServletRequest request){
        logger.info("with table request");
        try {
            ArticleResponse body = service.withTable(dto, request);
            return ResponseEntity.ok(body);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).build();
        } catch (Exception e){
            logger.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }
}
