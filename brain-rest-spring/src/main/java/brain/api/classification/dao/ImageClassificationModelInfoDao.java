package brain.api.classification.dao;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.data.model.ImageClassificationModelInfo;
import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.ImageClassificationModelInfoMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ImageClassificationModelInfoDao {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public ImageClassificationModelInfo getModelInfo(String model) {
        try (SqlSession session = sessionFactory.openSession(true)){
            logger.debug(model);
            ImageClassificationModelInfoMapper mapper = session.getMapper(ImageClassificationModelInfoMapper.class);
            return mapper.findByModel(model);
        }
    }
}
