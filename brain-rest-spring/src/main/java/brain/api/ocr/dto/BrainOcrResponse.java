package brain.api.ocr.dto;

import brain.api.common.data.CommonMsg;

import java.util.ArrayList;
import java.util.List;

public class BrainOcrResponse {
    private CommonMsg message;
    private List<BrainOcrTextBox> results;

    public BrainOcrResponse() {
        this.message = new CommonMsg();
        this.results = new ArrayList<>();
    }

    public BrainOcrResponse(CommonMsg message) {
        this.message = message;
    }

    public BrainOcrResponse(List<BrainOcrTextBox> results) {
        this.message = new CommonMsg();
        this.results = results;
    }

    public BrainOcrResponse(CommonMsg message, List<BrainOcrTextBox> results) {
        this.message = message;
        this.results = results;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List<BrainOcrTextBox> getResults() {
        return results;
    }

    public void setResults(List<BrainOcrTextBox> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "BrainOcrResponse{" +
                "message=" + message +
                ", results=" + results +
                '}';
    }
}
