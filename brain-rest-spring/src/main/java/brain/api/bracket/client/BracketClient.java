package brain.api.bracket.client;

import brain.api.bracket.BracketCommonCode;
import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.bracket.Bracket;
import maum.brain.bracket.BracketGRPCGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class BracketClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(BracketCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private BracketGRPCGrpc.BracketGRPCBlockingStub bracketBlockingStub;
    private BracketGRPCGrpc.BracketGRPCStub bracketStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if (super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.bracketBlockingStub = BracketGRPCGrpc.newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            this.bracketStub = BracketGRPCGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public String processing(MultipartFile file) throws InterruptedException {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        final int[] status = {-1};
        final String[] key = {null};
        // RESPONSE
        StreamObserver<Bracket.UploadMsg> responseObserver = new StreamObserver<Bracket.UploadMsg>() {
            @Override
            public void onNext(Bracket.UploadMsg uploadMsg) {
                status[0] = uploadMsg.getStatusCode();
                key[0] = uploadMsg.getFileKey();
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Response onError from Bracket processing");
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Response from Bracket processing");
                finishLatch.countDown();
            }
        };
        // STREAM REQUEST
        StreamObserver<Bracket.FileByte> requestObserver = bracketStub.processing(responseObserver);
        try {
            Bracket.FileByte.Builder builder = Bracket.FileByte.newBuilder();
            ByteString byteString = ByteString.copyFrom(file.getBytes());
            requestObserver.onNext(builder.setBuffer(byteString).build());
        } catch (IOException e) {
            logger.info("Error: Stream Request from Bracket processing");
        }
        requestObserver.onCompleted();
        finishLatch.await(1, TimeUnit.MINUTES);

        if(status[0] == 0) {
            return key[0];
        }else{
            throw new InternalError("Error: Bracket Engine");
        }
    }

    public ByteArrayOutputStream download(Iterator<Bracket.FileByte> responseStream){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            for (int i = 1; responseStream.hasNext(); i++) {
                Bracket.FileByte response = responseStream.next();
                outputStream.write(response.getBuffer().toByteArray());
            }
        }catch (Exception e){
            logger.info("Error: Stream response from Bracket downloadZip");
            logger.error(e.getMessage());
            throw new InternalException(e.getMessage());
        }
        return outputStream;
    }
    public ByteArrayOutputStream downloadZip(String key){
        // REQUEST
        Bracket.KeyMsg request = Bracket.KeyMsg.newBuilder().setFileKey(key).build();
        // STREAM RESPONSE
        Iterator<Bracket.FileByte> responseStream = bracketBlockingStub.downloadZip(request);
        return download(responseStream);
    }
    public ByteArrayOutputStream downloadAb(String key){
        // REQUEST
        Bracket.KeyMsg request = Bracket.KeyMsg.newBuilder().setFileKey(key).build();
        // STREAM RESPONSE
        Iterator<Bracket.FileByte> responseStream = bracketBlockingStub.downloadAB(request);
        return download(responseStream);
    }
    public ByteArrayOutputStream downloadRegion(String key){
        // REQUEST
        Bracket.KeyMsg request = Bracket.KeyMsg.newBuilder().setFileKey(key).build();
        // STREAM RESPONSE
        Iterator<Bracket.FileByte> responseStream = bracketBlockingStub.downloadRegion(request);
        return download(responseStream);
    }
}
