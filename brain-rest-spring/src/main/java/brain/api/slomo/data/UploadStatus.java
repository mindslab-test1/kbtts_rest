package brain.api.slomo.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import static brain.api.slomo.SuperSlomoCommonCode.SUCCESS;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UploadStatus implements Serializable {
    private int status;
    private String fileKey;
    private String message;

    public UploadStatus() {
    }

    public UploadStatus(int status, String fileKey, String errorMessage){
        switch (status){
            case 1:
                this.status = 200;
                this.fileKey = fileKey;
                this.message = SUCCESS;
                break;
            case 2:
                this.status = 500;
                this.message = errorMessage;
                break;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UploadStatus{" +
                "status=" + status +
                ", fileKey='" + fileKey + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
