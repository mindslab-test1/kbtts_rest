package brain.api.room3d.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class KeyMessageDTO {
    private String apiId;
    private String apiKey;
    private String fileKey;
}
