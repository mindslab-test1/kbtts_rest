package brain.api.stt.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.stt.SttCommonCode;
import brain.api.stt.data.CnnSttRes;
import brain.api.stt.data.StreamCnnSttRes;
import brain.api.stt.service.SttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/stt")
public class SttController {

    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    private SttService sttService;

    @RequestMapping(
            value = "/cnnStt",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE

    ) public @ResponseBody CnnSttRes cnnStt(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        try {
            return sttService.cnnStt(apiId, apiKey, file, httpRequest);
        } catch (MindsRestException | IOException e) {
            logger.error(e.getMessage());
            return new CnnSttRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @RequestMapping(
            value = "/cnnSttDetail",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public @ResponseBody StreamCnnSttRes streamCnnSttRes(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("lang") String lang,
            @RequestParam("model") String model,
            @RequestParam("samplerate") Integer samplerate,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ){
        try {
            return sttService.streamCnnStt(apiId, apiKey, lang, model, samplerate, file, httpRequest);
        } catch (MindsRestException | IOException e) {
            return new StreamCnnSttRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @RequestMapping(
            value = "/cnnSttScripted",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public @ResponseBody StreamCnnSttRes cnnSttWithTime(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("model") String modelName,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        try {
            return sttService.cnnSttScript(apiId, apiKey, modelName, file, httpRequest);
        } catch (MindsRestException e){
            logger.warn(e.getMessage());
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new StreamCnnSttRes(
                    e.createMessage()
            );
        } catch (IOException e) {
            logger.error(e.getMessage());
            httpResponse.setStatus(500);
            return new StreamCnnSttRes(
                    new MindsRestException().createMessage()
            );
        }
    }

    @PostMapping(
            value = "cnnSttSimple",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public @ResponseBody CnnSttRes test(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("model") String modelName,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        try {
            return sttService.cnnSttSimple(apiId, apiKey, modelName, file, httpRequest);
        } catch (MindsRestException e){
            logger.warn(e.getMessage());
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new CnnSttRes(
                    e.createMessage()
            );
        } catch (IOException e) {
            logger.error(e.getMessage());
            httpResponse.setStatus(500);
            return new CnnSttRes(
                    new MindsRestException().createMessage()
            );
        }
    }
}
