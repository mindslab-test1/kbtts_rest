package brain.api.ner.data;

import brain.api.common.data.CommonMsg;

import java.util.List;

public class NerBaseResponse {
    private CommonMsg message;
    private List<String> result;

    public NerBaseResponse(List<String> result){
        this.result = result;
        this.message = new CommonMsg();
    }

    public NerBaseResponse(CommonMsg message) {
        this.result = null;
        this.message = message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "NerBaseResponse{" +
                "message=" + message +
                ", result='" + result + '\'' +
                '}';
    }
}
