package brain.api.edueng.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.edueng.EduengCommonCode;
import brain.api.edueng.data.EduengRes;
import brain.api.edueng.data.EduengResult;
import brain.api.edueng.service.EduengService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/edueng")
public class EduengController {
    private static final CloudApiLogger logger = new CloudApiLogger(EduengCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);
    private final EduengService service;

    public EduengController(EduengService service) {
        this.service = service;
    }

    @RequestMapping(value = "/stt", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity eduengStt(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("userId") String userid,
            @RequestParam("model") String model,
            @RequestParam("answerText") String answerText,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        EduengResult result;
        try {
            logger.debug("Call eduengStt");
            result = service.eduengStt(apiId, apiKey, userid, model, answerText, file, httpRequest);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.valueOf(e.getErrCode() / 10));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new EduengRes(result), HttpStatus.OK);
    }

    @RequestMapping(value = "/pron", method = RequestMethod.POST, consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity eduengPron(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("userId") String userid,
            @RequestParam("model") String model,
            @RequestParam("answerText") String answerText,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        EduengResult result;
        try {
            logger.info("Call eduengPron");
            result = service.eduengPron(apiId, apiKey, userid, model, answerText, file, httpRequest);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.valueOf(e.getErrCode() / 10));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new EduengRes(result), HttpStatus.OK);
    }

    @RequestMapping(value = "/phonics", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity phonics(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("userId") String userid,
            @RequestParam("model") String model,
            @RequestParam("answerText") String answerText,
            @RequestParam("chkSym") String chkSym,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        EduengResult result;
        try {
            logger.debug("Call eduengPron");
            result = service.eduengPhonics(apiId, apiKey, userid, model, answerText, file, chkSym, httpRequest);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.valueOf(e.getErrCode() / 10));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new EduengRes(result), HttpStatus.OK);
    }
}

