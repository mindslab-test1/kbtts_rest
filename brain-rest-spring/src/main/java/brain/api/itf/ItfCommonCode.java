package brain.api.itf;

public class ItfCommonCode {
    private ItfCommonCode(){}

    public static final String SERVICE_NAME = "ITF";

    // Success
    public static final int ITF_RES_SUCCESS = 0;

    // Communication related error
    public static final int ITF_ERR_CONN_REFUSED = 50001;
    public static final int ITF_ERR_CONN_TIMEOUT = 50002;
    public static final int ITF_ERR_REQ_TIMEOUT = 50003;

    public static final int ITF_ERR_AUTH_VERIFICATION_ERROR = 40001;
}
