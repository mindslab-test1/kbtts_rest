package brain.api.common;

public class RedisCode {
    public static final String SERVICE_NAME = "REDIS";

    public static final String REDIS_HOST = "engine-api-1-redis-001.vl03nz.0001.apn2.cache.amazonaws.com";
    public static final int REDIS_PORT = 6379;

    private RedisCode() {}
}
