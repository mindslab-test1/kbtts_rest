package brain.api.correct.service;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.correct.CorrectCommonCode;
import brain.api.correct.client.CorrectClient;
import brain.api.correct.data.CorrectRes;
import brain.api.correct.model.CorrectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CorrectService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(CorrectCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    CorrectClient correctClient;

    public CorrectRes correctSentence(CorrectReq params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), CorrectCommonCode.SERVICE_NAME, 1, request);
        correctClient.setDestination("182.162.19.8",35025);
        return new CorrectRes(correctClient.GetSentenceSimple(params.getSentence()));
    }
}
