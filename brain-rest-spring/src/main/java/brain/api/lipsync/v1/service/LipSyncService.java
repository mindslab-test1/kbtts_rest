package brain.api.lipsync.v1.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.server.ChannelDto;
import brain.api.common.service.BaseService;
import brain.api.lipsync.LipSyncCommonCode;
import brain.api.lipsync.LipsyncKeyCollector;
import brain.api.lipsync.ResolutionType;
import brain.api.lipsync.v1.client.LipSyncClient;
import brain.api.lipsync.v1.data.LipSyncStatus;
import brain.api.lipsync.v1.data.LipsyncStatusCode;
import brain.api.lipsync.v1.dto.LipSyncDataResponse;
import brain.api.lipsync.v1.dto.LipSyncDownloadRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service("V1_LipSyncService")
public class LipSyncService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(LipSyncCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    private final LipSyncClient client;
    private final brain.api.lipsync.v2.client.LipSyncClient clientV2;
    private final LipsyncKeyCollector lipsyncKeyCollector;

    public LipSyncService(
            @Autowired @Qualifier("V1_LipSyncClient") LipSyncClient client,
            @Autowired brain.api.lipsync.v2.client.LipSyncClient clientV2,
            @Autowired LipsyncKeyCollector lipsyncKeyCollector
    ) {
        this.client = client;
        this.clientV2 = clientV2;
        this.lipsyncKeyCollector = lipsyncKeyCollector;
    }

    public LipSyncDataResponse upload(
            String apiId,
            String apikey,
            String text,
            String model,
            String resolution,
            boolean transparent,
            MultipartFile file, HttpServletRequest request
    ) throws MindsRestException, IOException, InterruptedException {
        LipSyncDataResponse lipSyncDataResponse;

        validate(apiId, apikey, LipSyncCommonCode.SERVICE_NAME, 1, request);
        if (text.trim().isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Please enter text");
        }
        ChannelDto dto = serverDao.getLipSyncServerInfo(model);
        if (dto == null) throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "no usable model present");

        logger.info("Sending to Engine: " + apiId + " => text= " + text + ", textByteLength= " + text.getBytes().length + ", model= "
                + model + ", version= " + dto.getVersion() + ", resolution= " + resolution + ", transparent= " + transparent);

        // TODO: 하위호환성을 위해 추가된 로직, 모델들 모두 이관 완료시 "/lipsync/v1" url 삭제할 에정
        if ("v2".equalsIgnoreCase(dto.getVersion())) {
            lipSyncDataResponse = new LipSyncDataResponse(new CommonMsg(LipSyncCommonCode.NOT_SUPPORT_V1_WARNING_MESSAGE), clientV2.upload(
                    text + LipSyncCommonCode.INFERENCE_DOT,
                    file == null ? null : file.getBytes(),
                    ResolutionType.valueOf(resolution).getResolution(),
                    transparent,
                    dto.getHost(),
                    dto.getPort(),
                    dto.getVersion()
            ));

            logger.info("Response Received " + apiId + " => text= " + text + ", textByteLength= " + text.getBytes().length + ", model= "
                    + model + ", version= " + dto.getVersion() + ", resolution= " + resolution + ", transparent= " + transparent + ", requestKey= " + lipSyncDataResponse.getPayload());
        } else {
            lipSyncDataResponse = new LipSyncDataResponse(new CommonMsg(LipSyncCommonCode.NOT_SUPPORT_V1_WARNING_MESSAGE), client.upload(
                    text + LipSyncCommonCode.INFERENCE_DOT,
                    file == null ? null : file.getBytes(),
                    resolution,
                    transparent,
                    dto.getHost(),
                    dto.getPort(),
                    dto.getVersion()
            ));

            logger.info("Response Received: " + apiId + " => text= " + text + ", textByteLength= " + text.getBytes().length + ", model= "
                    + model + ", version= " + dto.getVersion() + ", resolution= " + resolution + ", transparent= " + transparent + ", requestKey= " + lipSyncDataResponse.getPayload());
        }

        return lipSyncDataResponse;
    }

    public LipSyncDataResponse statusCheck(
            String apiId, String apiKey, String requestKey, HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, LipSyncCommonCode.SERVICE_NAME, 0, request);
        // TODO: 하위호환성을 위해 추가된 로직, 모델들 모두 이관 완료시 "/lipsync/v1" url 삭제할 에정
        String modelApiVersion = lipsyncKeyCollector.getByRequestKey(requestKey).getVersion();
        LipSyncStatus lipSyncStatus;
        CommonMsg commonMsg;
        if ("v2".equalsIgnoreCase(modelApiVersion)) {
            lipSyncStatus = clientV2.statusCheck(requestKey);
        } else {
            lipSyncStatus = client.statusCheck(requestKey);
        }
        switch (LipsyncStatusCode.fromOrdinal(lipSyncStatus.getStatusCode())) {
            case NOT_YET:
            case PROCESSING:
            case DONE:
                commonMsg = new CommonMsg(CommonCode.COMMON_SUCCESS, LipSyncCommonCode.NOT_SUPPORT_V1_WARNING_MESSAGE);
                break;
            case DELETED:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getErrCode()
                        , CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getMessage());
                break;
            case WRONG_KEY:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
                break;
            default:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
                break;
        }
        return new LipSyncDataResponse(commonMsg, lipSyncStatus);
    }

    public ByteArrayOutputStream download(
            LipSyncDownloadRequestDTO lipSyncDownloadRequestDTO,
            HttpServletRequest request
    ) throws MindsRestException {
        validate(lipSyncDownloadRequestDTO.getApiId(), lipSyncDownloadRequestDTO.getApiKey(),
                LipSyncCommonCode.SERVICE_NAME, 0, request);
        String modelApiVersion = lipsyncKeyCollector.getByRequestKey(lipSyncDownloadRequestDTO.getRequestKey()).getVersion();
        ByteArrayOutputStream byteArrayOutputStream;
        // TODO: 하위호환성을 위해 추가된 로직, 모델들 모두 이관 완료시 "/lipsync/v1" url 삭제할 에정
        if ("v2".equalsIgnoreCase(modelApiVersion)) {
            byteArrayOutputStream = clientV2.download(lipSyncDownloadRequestDTO.getRequestKey());
        } else {
            byteArrayOutputStream = client.download(lipSyncDownloadRequestDTO.getRequestKey());
        }

        logger.info("Download: " + lipSyncDownloadRequestDTO.getApiId() + " => " + lipSyncDownloadRequestDTO.getRequestKey());
        return byteArrayOutputStream;
    }
}
