package brain.api.txr.client;

import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.txr.data.TxrBaseResponse;
import org.springframework.web.multipart.MultipartFile;

public interface TxrClientInterface {
    public CommonMsg setDestination(String ip, int port);
    public TxrBaseResponse textRemoval(MultipartFile img) throws MindsRestException;
    public TxrBaseResponse textMasking(MultipartFile img) throws MindsRestException;
}
