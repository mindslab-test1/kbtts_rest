package brain.api.hmd.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HmdClassified implements Serializable {
    private int sentenceSequence;
    private String category;
    private String pattern;
    private String searchKey;
    private String sentence;

    public HmdClassified() {
    }

    public HmdClassified(int sentenceSequence, String category, String pattern, String searchKey, String sentence) {
        this.sentenceSequence = sentenceSequence;
        this.category = category;
        this.pattern = pattern;
        this.searchKey = searchKey;
        this.sentence = sentence;
    }

    public int getSentenceSequence() {
        return sentenceSequence;
    }

    public void setSentenceSequence(int sentenceSequence) {
        this.sentenceSequence = sentenceSequence;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public String toString() {
        return "HmdClassified{" +
                "sentenceSequence=" + sentenceSequence +
                ", category='" + category + '\'' +
                ", pattern='" + pattern + '\'' +
                ", searchKey='" + searchKey + '\'' +
                ", sentence='" + sentence + '\'' +
                '}';
    }
}
