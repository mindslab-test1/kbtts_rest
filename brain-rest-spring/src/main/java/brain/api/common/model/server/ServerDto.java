package brain.api.common.model.server;

public class ServerDto {
    private int id;
    private String service;
    private String model;
    private String ip;
    private int port;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getService() {
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "ServerDto{" +
                "id=" + id +
                ", service='" + service + '\'' +
                ", model='" + model + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
