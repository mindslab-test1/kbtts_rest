package brain.api.d2a.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WithTableDto implements Serializable {
    private String apiId;
    private String apiKey;
    private String title;
    private List<String> keys;
    private List<List<String>> values;
    private List<String> positions;

    public WithTableDto() {
        keys = new ArrayList<>();
        values = new ArrayList<>();
    }

    public WithTableDto(String apiId, String apiKey, String title, List<String> keys, List<List<String>> values, List<String> positions) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.title = title;
        this.keys = keys;
        this.values = values;
        this.positions = positions;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public List<List<String>> getValues() {
        return values;
    }

    public void setValues(List<List<String>> values) {
        this.values = values;
    }

    public List<String> getPositions() {
        return positions;
    }

    public void setPositions(List<String> positions) {
        this.positions = positions;
    }

    @Override
    public String toString() {
        return "WithTableDto{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", title='" + title + '\'' +
                ", keys=" + keys +
                ", values=" + values +
                ", positions=" + positions +
                '}';
    }
}
