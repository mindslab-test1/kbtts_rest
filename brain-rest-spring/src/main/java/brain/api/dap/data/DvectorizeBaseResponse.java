package brain.api.dap.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DvectorizeBaseResponse implements Serializable {
    private CommonMsg message;
    private List <Object> vector;

    public DvectorizeBaseResponse(int status) {
        this.message = new CommonMsg(status);
    }

    public DvectorizeBaseResponse(CommonMsg message, List <Object> vector) {
        this.message = message;
        this.vector = vector;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List <Object> getVector() {
        return vector;
    }

    public void setVector(List <Object> vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
        return "DvectorizeBaseResponse{" +
                "message=" + message +
                ", vector=" + vector +
                '}';
    }
}
