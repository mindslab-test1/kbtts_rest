package brain.api.smartx.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.service.BaseService;
import brain.api.smartx.SmartXCode;
import com.google.common.io.Files;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class SmartFeatureService extends BaseService {
    static final String url = SmartXCode.SMARTX_ADDRESS+SmartXCode.FEAT_SELECT;

    public Map<String, Object> getFeatureSelection(MultipartFile var_file, MultipartFile data_file, String apiId, String apiKey, HttpServletRequest request){
        Map<String, Object>  map = new LinkedHashMap<>();

        try{
            validate(apiId, apiKey, SmartXCode.FEAT_SELECT, 1, request);

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File varFile = Files.createTempDir();
            varFile = new File((varFile.getPath() + "/" + var_file.getOriginalFilename().substring(var_file.getOriginalFilename().lastIndexOf("\\") + 1)));
            var_file.transferTo(varFile);

            File dataFile = Files.createTempDir();
            dataFile = new File((dataFile.getPath() + "/" + data_file.getOriginalFilename().substring(data_file.getOriginalFilename().lastIndexOf("\\") + 1)));
            data_file.transferTo(dataFile);

            FileBody varFileBody = new FileBody(varFile);
            FileBody dataFileBody = new FileBody(dataFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addPart("var_file", varFileBody);
            builder.addPart("data_file", dataFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if (responseCode != 200) {
                throw new RuntimeException("# FeatureSelection >>  ErrCode : " + response);
            }

            InputStream in = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            int count = multipart.getCount();

            for(int i=0 ; i<count ; i++) {

                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                String name = getNameFromHeader(bodyPart);

                if(bodyPart.isMimeType("text/plain")){
                    BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream(), "UTF-8"));
                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while((line=rd.readLine()) != null) {
                        result.append(line + "\n");
                    }

                    String strResult = result.toString();
                    map.put(name, strResult);
                }
            }

            varFile.delete();
            dataFile.delete();
        }catch (Exception e){

        }

        return map;
    }

    String getNameFromHeader(BodyPart bodyPart){
        String result = "";

        try {
            Enumeration<Header> enumeration = bodyPart.getAllHeaders();
            while (enumeration.hasMoreElements()) {
                Header next = enumeration.nextElement();
                String name = next.getName();
                String value = next.getValue();

                if("Content-Disposition".equals(name)){
                    String[] headers = value.split(" ");
                    for(String header : headers){
                        if("name".equals(header.split("=")[0])){
                            result = header.split("=")[1].replaceAll("\"", "").replaceAll(";", "");
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }
}
