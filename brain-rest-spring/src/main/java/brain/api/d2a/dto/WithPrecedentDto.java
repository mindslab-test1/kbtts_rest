package brain.api.d2a.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WithPrecedentDto implements Serializable {
    private String apiId;
    private String apiKey;
    private List<String> values;

    public WithPrecedentDto() {
        values = new ArrayList<>();
    }

    public WithPrecedentDto(String apiId, String apiKey, List<String> values) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.values = values;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "WithPrecedentDto{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", values=" + values +
                '}';
    }
}
